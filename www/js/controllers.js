angular.module('starter.controllers', [])

    .run(function ($ionicPlatform) {
        $ionicPlatform.ready(function () {
            setTimeout(function () {
                // navigator.splashscreen.hide();
            }, 100);
        });
    })

    .controller('FriendsCtrl', function ($scope, Friends) {
        $scope.friends = Friends.all();
    })

    .controller('FriendDetailCtrl', function ($scope, $stateParams, Friends) {
        $scope.friend = Friends.get($stateParams.friendId);
    })

    .controller('AccountCtrl', function ($scope) {
    })
    .controller('productSortCtrl', function ($rootScope, $location, $scope, $http, $stateParams, category, retailers, brands, styles, $ionicScrollDelegate) {

        $scope.getRetailers = function () {
            //$rootScope.loader = true;
            retailers.get().then(function (data) {
                $scope.retailers = data;
                //  $rootScope.loader = false;
            });
        }

        $scope.getBrands = function () {
            //  $rootScope.loader = true;
            brands.get().then(function (data) {
                $scope.brands = data;
                // $rootScope.loader = false;
            });
        }
        $scope.isGroupShown = function (group) {
            return $scope.shownGroup === group;
        };
        $scope.getCategories = function () {
            // $rootScope.loader = true;
            category.get().then(function (data) {
                $scope.categories = data;
                //  $rootScope.loader = false;
            });
        }
        $scope.styles = styles.get();

        $scope.addMySelection = function ($item) {

            $.each($rootScope.mySelectionItem, function (index, obj) {
                if (obj.type == $item.itemType) {
                    $rootScope.mySelectionItem.splice(index, 1);
                }
            });
            $rootScope.mySelectionItem.push({name: $item.name, type: $item.itemType});

        }

        $scope.toggleGroup = function (group) {
            if ($scope.isGroupShown(group)) {
                $scope.shownGroup = null;
            } else {
                $scope.shownGroup = group;
            }
        };

        $scope.clear = function () {
            $rootScope.mySelectionItem = [];
        }


    })

    .controller('toptrendsCtrl', function ($rootScope, $location, $scope, $http, $stateParams, category, retailers, brands, styles, $ionicScrollDelegate, mainServices) {

        $rootScope.sttButton = false;
        $rootScope.slideHeader = false;
        $rootScope.slideHeaderPrevious = 0;

        $scope.scrollToTop = function () { //ng-click for back to top button

            $ionicScrollDelegate.scrollTop(5000);

        };


        $scope.remove = function (item) {
            if (item.type == 'Category') {
                $rootScope.cid = '';
            } else if (item.type == 'Provider') {
                $rootScope.rid = '';
            } else if (item.type == 'Manufacturer') {
                $rootScope.bid = '';
            } else if (item.type == 'style') {
                $rootScope.pid = '';
            }

            var index = $rootScope.mySelectionItem.indexOf(item)
            $rootScope.mySelectionItem.splice(index, 1);

            $location.url('app/tab/toptrends?pid=' + $rootScope.pid + '&cid=' + $rootScope.cid + '&bid=' + $rootScope.bid + '&rid=' + $rootScope.rid);

        }

        $rootScope.backbtn = false;
        $rootScope.togglebtn = true;
        $rootScope.searchlayoutFlag = true;
        //$rootScope.loader = true;
        var items = [];
        var newItems = [];
        $limit = 0;
        $scope.noMoreItemsAvailable = true;
        $scope.loadMore = function () {
            $scope.noMoreItemsAvailable = true;
           // $rootScope.loader = true;
            var $paramData = '';
            if ($stateParams.cid != null && $stateParams.cid != '' && $stateParams.cid != 'undefined') {
                $rootScope.cid = $stateParams.cid;
                $paramData += '&category=' + $scope.cid;
            } else {
                $rootScope.cid = '';
            }
            if ($stateParams.rid != null && $stateParams.rid != '' && $stateParams.rid != 'undefined') {
                $rootScope.rid = $stateParams.rid;
                $paramData += '&provider=' + $rootScope.rid;
            } else {
                $rootScope.rid = '';
            }
            if ($stateParams.bid != null && $stateParams.bid != '' && $stateParams.bid != 'undefined') {
                $rootScope.bid = $stateParams.bid;
                $paramData += '&brand=' + $rootScope.bid;
            } else {
                $rootScope.bid = '';
            }

            if ($stateParams.pid != null && $stateParams.pid != '' && $stateParams.pid != 'undefined') {
                $rootScope.pid = $stateParams.pid
                $paramData += '&persona=' + $rootScope.pid;
            } else {
                $rootScope.pid = '';
            }
            if ($stateParams.sortType != null && $stateParams.sortType != '' && $stateParams.sortType != 'undefined') {
                $rootScope.sortType = $stateParams.sortType
                $paramData += '&sortType=' + $rootScope.sortType;
            } else {
                $rootScope.sortType = '';
            }
            var  $serviceName = 'trends';
            if ($stateParams.q != null && $stateParams.q != '' && $stateParams.sortType != 'undefined') {
                $rootScope.q = $stateParams.q
                $paramData += '&q=' + $rootScope.q;
                $serviceName = 'product/search'
            } else {
                $rootScope.q = '';
                $serviceName = 'trends';
            }


            mainServices.product.get($limit,$paramData,$serviceName).then(function (data) {
                console.log(data);
                if (data.status == 200) {
                    newItems = data.response;
                    if (data.response.length == 0)
                        $scope.noMoreItemsAvailable = false;

                    $.each(newItems, function (index, value) {
                        items.push(value);
                    });
                    $scope.toptrendsList = items;

                    $limit = $limit + $rootScope.limitCount;
                    $scope.$broadcast('scroll.infiniteScrollComplete');
                } else {
                    $rootScope.showErrAlert();
                    // showLongTop('Highe');
                    // $rootScope.loader = false;
                }

            });
        };

    })
    .controller('topVideosCtrl', function ($rootScope, $scope, $stateParams, $http, profile, $state, mainServices) {
        //For Tabs
        $('.left-menu').find('a').removeClass('active');
        $('.toptrendbrewers').addClass('active');
        $rootScope.leftMenuItem = 'toptrendbrewers';
        $rootScope.searchlayoutFlag = true;
        //$rootScope.backbtn = false;
        $rootScope.togglebtn = true;
        $scope.isLoggedIn = false;
        $rootScope.filter = true;
        $rootScope.filtercontent = false;
        $scope.predicate = 'numFollowers';


        $scope.videoDetail = function (videoData) {
            $rootScope.videoDetail = videoData;
        }

        $scope.addVideo = function (videoData) {
            profile.addVideo($rootScope.consumerId, videoData);
            $scope.videoData = {};
            $scope.videoDetail();
            $state.go('app.tabs.topvideos');
        }
        var $limit = 0;
        var items = [];
        var newItems = [];
        var $sortType = '';
        if ($stateParams.sortType != null && $stateParams.sortType != '' && $stateParams.sortType != 'undefined') {
            $sortType = $stateParams.sortType;
        }
        $scope.noMoreItemsAvailable = true;
        $scope.loadMore = function () {
            //$rootScope.loader = true;
            mainServices.topVideos($limit, $sortType).then(function (data) {
                newItems = data;
                if (data.length == 0)
                    $scope.noMoreItemsAvailable = false;

                $.each(newItems, function (index, value) {
                    items.push(value);
                });

                $limit = $limit + $rootScope.limitCount;
                $scope.$broadcast('scroll.infiniteScrollComplete');
                $scope.topVideos = items;
                //$rootScope.loader = false;
            });
        }

    })
    .controller('topBlogsCtrl', function ($rootScope, $scope, $stateParams, $http, swiperPlugin, profile, $state, mainServices) {

        $('.left-menu').find('a').removeClass('active');
        $('.shoppingcatalog').addClass('active');
        $rootScope.leftMenuItem = 'shoppingcatalog';

        // $rootScope.backbtn = false;
        $rootScope.togglebtn = true;
        $scope.searchFlag = false;
        $scope.breadcrumb = "Select a category";
        var mytrendUser = "";
        if ($rootScope.mytrend) {
            var userId = '';
            if ($rootScope.consumerId != '' && $rootScope.consumerId != undefined) {
                userId = $rootScope.consumerId;
            }
            mytrendUser = "?userId=" + userId;
        }


        $scope.blogDetails = function (bolgData) {
            console.log(bolgData);
            $rootScope.blogDetail = bolgData;
        }

        $scope.addBlog = function (blogData) {
            profile.addBlog($rootScope.consumerId, blogData);
            $scope.blogDetails();
            $state.go('app.tabs.topblogs');

        }

        var $limit = 1;
        var items = [];
        var newItems = [];
        var $sortType = '';
        if ($stateParams.sortType != null && $stateParams.sortType != '' && $stateParams.sortType != 'undefined') {
            $sortType = $stateParams.sortType;
        }
        $scope.noMoreItemsAvailable = true;
        $scope.loadMore = function () {
            //$rootScope.loader = true;
            mainServices.topBlogs($limit, $sortType).then(function (data) {
                newItems = data;
                if (data.length == 0)
                    $scope.noMoreItemsAvailable = false;

                $.each(newItems, function (index, value) {
                    items.push(value);
                });

                $limit = $limit + 24;
                $scope.$broadcast('scroll.infiniteScrollComplete');
                $scope.tobBlogs = items;
                $rootScope.loader = false;
            });
        }
    })
    .controller('myBlogsCtrl', function ($rootScope, $scope, $stateParams, $http, swiperPlugin, profile, $state) {
        $rootScope.backbtn = true;
        $rootScope.togglebtn = true;
        $scope.searchFlag = false;
        $scope.brewId = $stateParams.brewId;
        $scope.blogDetails = function (bolgData) {
            $rootScope.blogDetail = bolgData;
        }

        $scope.addBlog = function (blogData) {
            profile.addBlog($rootScope.consumerId, blogData);
            $state.go('app.tabs.blogs', {brewId: $scope.brewId});
        }
        profile.myBlogs($scope.brewId).then(function (response) {
            $scope.brewBlogs = response;
        });
    })
    .controller('productviewdetailsCtrl', function ($scope, $http, Auth, $rootScope, $ionicModal, $ionicSlideBoxDelegate, $ionicScrollDelegate, $stateParams, addMyTrend, profile, mainServices) {

        $scope.productId = $stateParams.productId;

        mainServices.product.productDetails($scope.productId).then(function (data) {
            console.log(data);
            if (data.status == 200) {
                $scope.productDetails = data.response;
                $rootScope.productBlogs = data.response.blogs;
                $ionicSlideBoxDelegate.update();
                // alert('rating');
                if ($scope.productDetails.averageRating > 0) {
                    $rating = (($scope.productDetails.averageRating / 5) * 100);
                    //alert($rating);
                    jQuery('.star-rating i').css({'width': $rating + '%', 'opacity': '1'});
                }
            }
        });

        $scope.email = function () {
            window.open('mailto:?subject=TrendBrew Curated Social Commerce Network Brew Trends...Make Money!&body=' + siteUrl + 'brewDetails/' + $scope.productId, '_system', 'location=yes');
            return false;
        }
        $scope.twitter = function () {
            window.open('https://twitter.com/share?url=' + siteUrl + 'brewDetails/' + $scope.productId + '&text=TrendBrew Curated Social Commerce Network Brew Trends...Make Money!', '_system', 'location=yes');
            return false;
        }
        $scope.facebook = function () {
            window.open('https://facebook.com/sharer/sharer.php?u=' + siteUrl + 'brewDetails/' + $scope.productId, '_system', 'location=yes');
            return false;
        }
        $scope.googleplus = function () {
            window.open('https://plus.google.com/share?url=' + siteUrl + 'brewDetails/' + $scope.productId, '_system', 'location=yes');
            return false;
        }

        $ionicModal.fromTemplateUrl('popupratingdetails.html', function ($ionicModal) {
            $scope.rating = $ionicModal;
        }, {
            // Use our scope for the scope of the modal to keep it simple
            scope: $scope,
            // The animation we want to use for the modal entrance
            animation: 'slide-in-up'
        });
        $scope.externalUrl = function (url,dealId) {
            if($rootScope.consumerId != undefined && $rootScope.consumerId != ''){
                mainServices.externalUrlCheckout(dealId,$rootScope.consumerId).then(function (response) {
                    console.log(response);
                });
            }
            $rootScope.openExternalUrl(url);
        }
        $scope.showConfirmRating = function ($rating) {
            $scope.ratingScore = $rating;
            $scope.review = null;
            $scope.rating.show();
        }

        $scope.reviewPost = function ($reviewData, $itemId) {
            var $reviewObj = {};
            $reviewObj.review = $reviewData.review;
            $reviewObj.rating = $scope.ratingScore;
            $reviewObj.itemId = $itemId;
            $reviewObj.reviewerId = $rootScope.consumerId;
            profile.productReview($rootScope.consumerId, $reviewObj).then(function (data) {
                $scope.productDetails.reviews = data;
                $scope.rating.hide();
            });

        }

        $ionicModal.fromTemplateUrl('popuptrenddetails.html', function ($ionicModal) {
            $scope.trend = $ionicModal;
        }, {
            // Use our scope for the scope of the modal to keep it simple
            scope: $scope,
            // The animation we want to use for the modal entrance
            animation: 'slide-in-up'
        });
        $scope.showConfirmtrend = function () {
            addMyTrend.BrewTrend($rootScope.consumerId, $stateParams.productId).then(function (data) {
                console.log(data);
                $scope.trend.show();
            });
        }

        $ionicModal.fromTemplateUrl('popupwishdetails.html', function ($ionicModal) {
            $scope.wish = $ionicModal;
        }, {
            // Use our scope for the scope of the modal to keep it simple
            scope: $scope,
            // The animation we want to use for the modal entrance
            animation: 'slide-in-up'
        });
        $scope.showConfirmwish = function () {
            $scope.wish.show();
        }

        $scope.submitWish = function (wishData) {
            console.log(wishData);
            addMyTrend.BrewWish($rootScope.consumerId, $stateParams.productId, wishData).then(function (data) {
                console.log(data);
                $scope.wishData = {};
                $scope.wish.hide();
            });

        }

        $ionicModal.fromTemplateUrl('popuplikedetails.html', function ($ionicModal) {
            $scope.like = $ionicModal;
        }, {
            // Use our scope for the scope of the modal to keep it simple
            scope: $scope,
            // The animation we want to use for the modal entrance
            animation: 'slide-in-up'
        });
        $scope.showConfirmlike = function () {
            addMyTrend.brewLike($rootScope.consumerId, $stateParams.productId).then(function (data) {
                console.log(data);
                $scope.like.show();
            });

        }

        $ionicModal.fromTemplateUrl('popupbuydetails.html', function ($ionicModal) {
            $scope.buy = $ionicModal;
        }, {
            // Use our scope for the scope of the modal to keep it simple
            scope: $scope,
            // The animation we want to use for the modal entrance
            animation: 'slide-in-up'
        });
        $scope.showbuy = function ($offer) {
            $scope.itemSource = $offer.sourceUrl;
            $scope.dealId = $offer.dealId;
            $scope.buy.show();
        }

        $ionicModal.fromTemplateUrl('popupwishdetails.html', function ($ionicModal) {
            $scope.wish = $ionicModal;
        }, {
            // Use our scope for the scope of the modal to keep it simple
            scope: $scope,
            // The animation we want to use for the modal entrance
            animation: 'slide-in-up'
        });
        $scope.showConfirmwish = function () {
            $scope.wish.show();
        }


        $ionicModal.fromTemplateUrl('popupbuzzdetails.html', function ($ionicModal) {
                $scope.modal = $ionicModal;
            },
            {
                // Use our scope for the scope of the modal to keep it simple
                scope: $scope,
                // The animation we want to use for the modal entrance
                animation: 'slide-in-up'
            });
        $scope.showConfirm = function () {
            $scope.modal.show();
        }
        $scope.submitBuzz = function (buzzData) {
            addMyTrend.BrewBuzz($rootScope.consumerId, $stateParams.productId, buzzData).then(function (data) {
                console.log(data);
                $scope.buzz = {};
                $scope.modal.hide();
            });

        }

        $scope.openPopover = function ($event) {
            $scope.popover.show($event);
        };

        $scope.scrollTo = function (fromTop) {
            $ionicScrollDelegate.scrollTo(0, fromTop, [true]);
            $scope.popover.hide();
        };
        //$scope.myActiveSlide = index;
        $scope.nextSlide1 = function () {
            $ionicSlideBoxDelegate.$getByHandle('burgers1').next();
        }
        $scope.previousSlide1 = function () {
            $ionicSlideBoxDelegate.$getByHandle('burgers1').previous();
        }
        $scope.nextSlide2 = function () {
            $ionicSlideBoxDelegate.$getByHandle('burgers2').next();
        }
        $scope.previousSlide2 = function () {
            $ionicSlideBoxDelegate.$getByHandle('burgers2').previous();
        }
        $scope.nextSlide3 = function () {
            $ionicSlideBoxDelegate.$getByHandle('burgers3').next();
        }
        $scope.previousSlide3 = function () {
            $ionicSlideBoxDelegate.$getByHandle('burgers3').previous();
        }
        //$ionicSlideBoxDelegate.$getByHandle('burgers').next();
        $rootScope.backbtn = true;
        $rootScope.togglebtn = true;
        $rootScope.searchlayoutFlag = true;
        var mytrendUser = "";
        if ($rootScope.mytrend) {
            var userId = '';
            if ($rootScope.consumerId != '' && $rootScope.consumerId != undefined) {
                userId = $rootScope.consumerId;
            }
            mytrendUser = "?userId=" + userId;
        }
        $scope.searchFlag = false;
        if ($rootScope.mytrend)
            $scope.breadcrumb = "My Trends";
        else
            $scope.breadcrumb = "Top Trends";


        $scope.toggleGroup = function (group) {
            $ionicSlideBoxDelegate.update();
            if ($scope.isGroupShown(group)) {
                $scope.shownGroup = null;
            } else {
                $scope.shownGroup = group;
            }
        };
        $scope.isGroupShown = function (group) {
            return $scope.shownGroup === group;
        };

    })
    .controller('mytrendstoreCtrl', function ($scope, $http, Auth, $rootScope, $stateParams, profile) {
        $('.left-menu').find('a').removeClass('active');
        $('.toptrendbrewers').addClass('active');
        $rootScope.leftMenuItem = 'toptrendbrewers';
        $rootScope.searchlayoutFlag = true;
        $rootScope.backbtn = false;
        $rootScope.togglebtn = true;
        $scope.isLoggedIn = false;
        $rootScope.filter = true;
        $rootScope.filtercontent = false;
        $scope.storeCount = 0;
        profile.get($stateParams.brewId).then(function (data) {
            $rootScope.profile = data;
        });
        $scope.brewerId = $stateParams.brewerId;
    })

    .controller('brewfeedCtrl', function ($rootScope, $scope, $ionicModal, $ionicPopover, profile, nowTime, $stateParams) {
        $scope.nowTime = nowTime;
        $rootScope.backbtn = true;
        $scope.brewId = $stateParams.brewId;

        $ionicModal.fromTemplateUrl('comment_modal.html', function($ionicModal) {
            $scope.comment_modal = $ionicModal;
        }, {
            // Use our scope for the scope of the modal to keep it simple
            scope: $scope,
            // The animation we want to use for the modal entrance
            animation: 'slide-in-up'
        });
        $scope.comment_modal_open = function($eventBuzz){

                $scope.commentBuzz =$eventBuzz;
            $scope.comment_modal.show();
        }


        $scope.email = function ($productId) {
            window.open('mailto:?subject=TrendBrew Curated Social Commerce Network Brew Trends...Make Money!&body=' + siteUrl + 'brewDetails/' + $productId, '_system', 'location=yes');
            return false;
        }
        $scope.twitter = function ($productId) {
            window.open('https://twitter.com/share?url=' + siteUrl + 'brewDetails/' + $productId + '&text=TrendBrew Curated Social Commerce Network Brew Trends...Make Money!', '_system', 'location=yes');
            return false;
        }
        $scope.facebook = function ($productId) {
            window.open('https://facebook.com/sharer/sharer.php?u=' + siteUrl + 'brewDetails/' + $productId, '_system', 'location=yes');
            return false;
        }
        $scope.googleplus = function ($productId) {
            window.open('https://plus.google.com/share?url=' + siteUrl + 'brewDetails/' + $productId, '_system', 'location=yes');
            return false;
        }

        $scope.postComment = function(feed){
            profile.feeds.comment($stateParams.brewId,feed.eventId,feed.comment).then(function(data){
                feed.comment = '';
            });
        }


        var $limit = 0;
        var items = [];
        var newItems = [];
        $scope.noMoreItemsAvailable = true;

        $scope.feedLike = function (feed) {
            profile.feeds.like($stateParams.brewId, feed.eventId).then(function(data){
                feed.numLikes += 1;
            });


        }

        $scope.loadMore = function () {
            $scope.noMoreItemsAvailable = true;
            $rootScope.loader = true;

            profile.feeds.get($stateParams.brewId, $limit).then(function (data) {
                //   $scope.feedsDetails = data;
                console.log(data);
                $rootScope.loader = false;
                $rootScope.loader = false;
                if (data.status == 200) {
                    newItems = data.response;
                    if (data.response.length == 0)
                        $scope.noMoreItemsAvailable = false;

                    $.each(newItems, function (index, value) {
                        items.push(value);
                    });

                    $scope.feedsDetails = items;

                    $limit = $limit + $rootScope.limitCount;
                    $scope.$broadcast('scroll.infiniteScrollComplete');
                } else {
                    // showLongTop('Highe');
                    $rootScope.loader = false;
                }
            });
        }

        $scope.getIcons = function (type) {
            // Make this more dynamic, but you get the idea
            switch (type) {
                case "BWISH":
                    return 'img/product-icons/bwish.png';
                    break;
                case "BTRND":
                    return 'img/product-icons/btrnd.png';
                    break;
                case "EBP":
                    return 'img/product-icons/ebp.png';
                    break;
                case "BLIKE":
                    return 'img/product-icons/blike.png';
                    break;
                case "SIGN":
                    return 'img/product-icons/sign.png';
                    break;
                case "SCON":
                    return 'img/product-icons/bwish.png';
                    break;
                case "UVID":
                    return 'img/product-icons/uvid.png';
                    break;
                case "UBLG":
                    return 'img/product-icons/ublg.png';
                    break;
                default :
                    return 'img/product-icons/bwish.png';
                    break;
            }
        }

    })
    .controller('followersCtrl', function ($rootScope, $scope, swiperPlugin, $ionicPopover, $ionicModal, profile, $stateParams) {
        $rootScope.backbtn = true;
        $scope.brewId = $stateParams.brewId;
        function loadPage() {
            $rootScope.loader = true;
            profile.followers($stateParams.brewId).then(function (data) {
                $scope.followersDetails = data;
                $rootScope.loader = false;
            });
        }

        loadPage();
    })
    .controller('followingCtrl', function ($rootScope, $scope, profile, $stateParams) {
        $rootScope.backbtn = true;
        $scope.brewId = $stateParams.brewId;
        function loadPage() {
            $rootScope.loader = true;
            profile.following($stateParams.brewId).then(function (data) {
                $scope.followingDetails = data;
                $rootScope.loader = false;
            });
        }

        loadPage();
    })
    .controller('mywishesCtrl', function ($scope, $rootScope, $stateParams, $http, $location) {


        $rootScope.backbtn = false;
        $rootScope.togglebtn = true;
        $rootScope.searchlayoutFlag = true;
        $scope.brewId = $stateParams.brewId;
        //$rootScope.loader = true;
        var items = [];
        var newItems = [];
        $limit = 0;
        $scope.noMoreItemsAvailable = true;

        $scope.remove = function (item) {
            if (item.type == 'Category') {
                $rootScope.cid = '';
            } else if (item.type == 'Provider') {
                $rootScope.rid = '';
            } else if (item.type == 'Manufacturer') {
                $rootScope.bid = '';
            } else if (item.type == 'style') {
                $rootScope.pid = '';
            }

            var index = $rootScope.mySelectionItem.indexOf(item)
            $rootScope.mySelectionItem.splice(index, 1);

            $location.url('app/tab/mywishes/' + $scope.brewId + '?pid=' + $rootScope.pid + '&cid=' + $rootScope.cid + '&bid=' + $rootScope.bid + '&rid=' + $rootScope.rid);

        }
        $scope.loadMore = function () {

            $scope.noMoreItemsAvailable = true;

            var $paramData = '';
            if ($stateParams.cid != null && $stateParams.cid != '' && $stateParams.cid != 'undefined') {
                $rootScope.cid = $stateParams.cid;
                $paramData += '&category=' + $scope.cid;
            } else {
                $rootScope.cid = '';
            }
            if ($stateParams.rid != null && $stateParams.rid != '' && $stateParams.rid != 'undefined') {
                $rootScope.rid = $stateParams.rid;
                $paramData += '&provider=' + $rootScope.rid;
            } else {
                $rootScope.rid = '';
            }
            if ($stateParams.bid != null && $stateParams.bid != '' && $stateParams.bid != 'undefined') {
                $rootScope.bid = $stateParams.bid;
                $paramData += '&brand=' + $rootScope.bid;
            } else {
                $rootScope.bid = '';
            }

            if ($stateParams.pid != null && $stateParams.pid != '' && $stateParams.pid != 'undefined') {
                $rootScope.pid = $stateParams.pid
                $paramData += '&persona=' + $rootScope.pid;
            } else {
                $rootScope.pid = '';
            }
            if ($stateParams.sortType != null && $stateParams.sortType != '' && $stateParams.sortType != 'undefined') {
                $rootScope.sortType = $stateParams.sortType
                $paramData += '&sortType=' + $rootScope.sortType;
            } else {
                $rootScope.sortType = '';
            }

            $rootScope.loader = true;
            $http.get(serviceURL + 'brewer/wishes?brewerId=' + $scope.brewId + '&from=' + $limit + $paramData, {cache: true}).error(function () {
                $rootScope.loader = false;
                $rootScope.showErrAlert();
            }).success(function (data, status, headers, config) {
                $rootScope.loader = false;
                if (data.status == 200) {
                    newItems = data.response;
                    if (data.response.length == 0)
                        $scope.noMoreItemsAvailable = false;

                    $.each(newItems, function (index, value) {
                        items.push(value);
                    });
                    $scope.wishes = items;

                    $limit = $limit + 25;
                    $scope.$broadcast('scroll.infiniteScrollComplete');
                } else {
                    // showLongTop('Highe');
                    $rootScope.loader = false;
                }
            });
        };
        //$rootScope.loader = true;
        //$scope.brewId = $stateParams.brewId;
        //profile.wishes($stateParams.brewId).then(function (data) {
        //    $scope.wishes = data;
        //    $rootScope.loader = false;
        //});
    })
    .controller('brewersProfileCtrl', function ($rootScope, $scope, $stateParams, profile) {
        $rootScope.backbtn = true;
        $scope.brewId = $stateParams.brewId;
        function loadPage() {
            $rootScope.loader = true;
            profile.get($stateParams.brewId).then(function (data) {
                $rootScope.brewersProfile = data;
                $rootScope.loader = false;
            });
        }

        loadPage();
    })
    .controller('mystoreCtrl', function ($rootScope, $scope, swiperPlugin, $ionicPopover, $ionicModal, $state, $stateParams, profile) {
        $scope.whatsappShare = function () {
            window.plugins.socialsharing.shareViaWhatsApp('Digital Signature Maker', null /* img */, "https://play.google.com/store/apps/details?id=com.prantikv.digitalsignaturemaker" /* url */, null, function (errormsg) {
                alert("Error: Cannot Share")
            });
        }
        $scope.facebookShare = function () {
            window.plugins.socialsharing.shareViaFacebook('Digital Signature Maker', null /* img */, "https://play.google.com/store/apps/details?id=com.prantikv.digitalsignaturemaker" /* url */, null, function (errormsg) {
                alert("Error: Cannot Share")
            });
        }
        $scope.twitterShare = function () {
            window.plugins.socialsharing.shareViaTwitter('Digital Signature Maker', null /* img */, 'https://play.google.com/store/apps/details?id=com.prantikv.digitalsignaturemaker', null, function (errormsg) {
                alert("Error: Cannot Share")
            });
        }
        $scope.OtherShare = function () {
            window.plugins.socialsharing.share('Digital Signature Maker', null, null, 'https://play.google.com/store/apps/details?id=com.prantikv.digitalsignaturemaker');
        }
        $rootScope.backbtn = true;
        $scope.brewId = $stateParams.brewId;
        profile.brewerStores($stateParams.brewId).then(function (data) {
            $scope.brewerStores = data;
        });

    })
    .controller('myoffersCtrl', function ($rootScope, $scope, swiperPlugin, $ionicPopover, $ionicModal, $state, profile, $stateParams) {
        $rootScope.loader = true;
        $scope.brewId = $stateParams.brewId;
        profile.offers($stateParams.brewId).then(function (data) {
            $scope.offers = data;
            $rootScope.loader = false;
        });
        $ionicModal.fromTemplateUrl('templates/product-modal.html', function ($ionicModal) {
            $scope.modal = $ionicModal;
        }, {
            // Use our scope for the scope of the modal to keep it simple
            scope: $scope,
            // The animation we want to use for the modal entrance
            animation: 'slide-in-up'
        });
        $scope.showConfirm = function ($offerItem) {
            $scope.offerItem = $offerItem;
            console.log($offerItem);
            $scope.modal.show();
        }
    })
    .controller('mybrewpointsCtrl', function ($rootScope, $scope, profile, $stateParams) {
        $rootScope.backbtn = true;
        $scope.brewId = $stateParams.brewId;
        profile.brewPoints($stateParams.brewId).then(function (data) {
            $scope.brewPoins = data;
            console.log('respon-d-', data);
        });
        profile.pointsTransactions($stateParams.brewId).then(function (data) {
            $scope.pointsTransactions = data;
            console.log(data);
        });
        profile.myTransaction($stateParams.brewId).then(function (data) {
            $scope.myTransaction = data;
            console.log(data);
        });
        profile.storeTransactions($stateParams.brewId).then(function (data) {
            $scope.storeTransactions = data;
            console.log(data);
        });
    })

    .controller('brewsCtrl', function ($scope, $rootScope, $stateParams, $http, $location) {

        $rootScope.backbtn = false;
        $rootScope.togglebtn = true;
        $rootScope.searchlayoutFlag = true;
        $scope.brewId = $stateParams.brewId;
        //$rootScope.loader = true;
        var items = [];
        var newItems = [];
        $limit = 0;
        $scope.noMoreItemsAvailable = true;

        $scope.remove = function (item) {
            if (item.type == 'Category') {
                $rootScope.cid = '';
            } else if (item.type == 'Provider') {
                $rootScope.rid = '';
            } else if (item.type == 'Manufacturer') {
                $rootScope.bid = '';
            } else if (item.type == 'style') {
                $rootScope.pid = '';
            }

            var index = $rootScope.mySelectionItem.indexOf(item)
            $rootScope.mySelectionItem.splice(index, 1);

            $location.url('app/tab/brews/' + $scope.brewId + '?pid=' + $rootScope.pid + '&cid=' + $rootScope.cid + '&bid=' + $rootScope.bid + '&rid=' + $rootScope.rid);

        }
        $scope.loadMore = function () {

            $scope.noMoreItemsAvailable = true;

            var $paramData = '';
            if ($stateParams.cid != null && $stateParams.cid != '' && $stateParams.cid != 'undefined') {
                $rootScope.cid = $stateParams.cid;
                $paramData += '&category=' + $scope.cid;
            } else {
                $rootScope.cid = '';
            }
            if ($stateParams.rid != null && $stateParams.rid != '' && $stateParams.rid != 'undefined') {
                $rootScope.rid = $stateParams.rid;
                $paramData += '&provider=' + $rootScope.rid;
            } else {
                $rootScope.rid = '';
            }
            if ($stateParams.bid != null && $stateParams.bid != '' && $stateParams.bid != 'undefined') {
                $rootScope.bid = $stateParams.bid;
                $paramData += '&brand=' + $rootScope.bid;
            } else {
                $rootScope.bid = '';
            }

            if ($stateParams.pid != null && $stateParams.pid != '' && $stateParams.pid != 'undefined') {
                $rootScope.pid = $stateParams.pid
                $paramData += '&persona=' + $rootScope.pid;
            } else {
                $rootScope.pid = '';
            }
            if ($stateParams.sortType != null && $stateParams.sortType != '' && $stateParams.sortType != 'undefined') {
                $rootScope.sortType = $stateParams.sortType
                $paramData += '&sortType=' + $rootScope.sortType;
            } else {
                $rootScope.sortType = '';
            }

            $rootScope.loader = true;
            $http.get(serviceURL + 'brewer/brews?brewerId=' + $scope.brewId + '&from=' + $limit + $paramData, {cache: true}).error(function () {
                $rootScope.loader = false;
                $rootScope.showErrAlert();
            }).success(function (data, status, headers, config) {
                $rootScope.loader = false;
                if (data.status == 200) {
                    newItems = data.response;
                    if (data.response.length == 0)
                        $scope.noMoreItemsAvailable = false;

                    $.each(newItems, function (index, value) {
                        items.push(value);
                    });
                    $scope.brewerBrewers = items;

                    $limit = $limit + 25;
                    $scope.$broadcast('scroll.infiniteScrollComplete');
                } else {
                    // showLongTop('Highe');
                    $rootScope.loader = false;
                }
            });
        };


        //$rootScope.backbtn = true;
        //
        //mainServices.brewerBrewers($scope.brewId).then(function(response){
        //    $scope.brewerBrewers = response;
        //});
    })
    .controller('storeBrewsCtrl', function ($scope, $rootScope, $stateParams, $location, $http) {

        $rootScope.backbtn = false;
        $rootScope.togglebtn = true;
        $rootScope.searchlayoutFlag = true;
        $scope.brewId = $stateParams.brewId;
        //$rootScope.loader = true;
        var items = [];
        var newItems = [];
        $limit = 0;
        $scope.noMoreItemsAvailable = true;

        $scope.remove = function (item) {
            if (item.type == 'Category') {
                $rootScope.cid = '';
            } else if (item.type == 'Provider') {
                $rootScope.rid = '';
            } else if (item.type == 'Manufacturer') {
                $rootScope.bid = '';
            } else if (item.type == 'style') {
                $rootScope.pid = '';
            }

            var index = $rootScope.mySelectionItem.indexOf(item)
            $rootScope.mySelectionItem.splice(index, 1);

            $location.url('app/tab/brews/' + $scope.brewId + '?pid=' + $rootScope.pid + '&cid=' + $rootScope.cid + '&bid=' + $rootScope.bid + '&rid=' + $rootScope.rid);

        }
        $scope.loadMore = function () {

            $scope.noMoreItemsAvailable = true;

            var $paramData = '';
            if ($stateParams.cid != null && $stateParams.cid != '' && $stateParams.cid != 'undefined') {
                $rootScope.cid = $stateParams.cid;
                $paramData += '&category=' + $scope.cid;
            } else {
                $rootScope.cid = '';
            }
            if ($stateParams.rid != null && $stateParams.rid != '' && $stateParams.rid != 'undefined') {
                $rootScope.rid = $stateParams.rid;
                $paramData += '&provider=' + $rootScope.rid;
            } else {
                $rootScope.rid = '';
            }
            if ($stateParams.bid != null && $stateParams.bid != '' && $stateParams.bid != 'undefined') {
                $rootScope.bid = $stateParams.bid;
                $paramData += '&brand=' + $rootScope.bid;
            } else {
                $rootScope.bid = '';
            }

            if ($stateParams.pid != null && $stateParams.pid != '' && $stateParams.pid != 'undefined') {
                $rootScope.pid = $stateParams.pid
                $paramData += '&persona=' + $rootScope.pid;
            } else {
                $rootScope.pid = '';
            }
            if ($stateParams.sortType != null && $stateParams.sortType != '' && $stateParams.sortType != 'undefined') {
                $rootScope.sortType = $stateParams.sortType
                $paramData += '&sortType=' + $rootScope.sortType;
            } else {
                $rootScope.sortType = '';
            }

            $rootScope.loader = true;
            $http.get(serviceURL + 'storeItems?storeId=' + $scope.brewId + '&brewerId=' + $scope.brewId + '&from=' + $limit + $paramData, {cache: true}).error(function () {
                $rootScope.loader = false;
                $rootScope.showErrAlert();
            }).success(function (data, status, headers, config) {
                $rootScope.loader = false;
                if (data.status == 200) {
                    newItems = data.response;
                    if (data.response.length == 0)
                        $scope.noMoreItemsAvailable = false;

                    $.each(newItems, function (index, value) {
                        items.push(value);
                    });
                    $scope.brewerBrewers = items;

                    $limit = $limit + 25;
                    $scope.$broadcast('scroll.infiniteScrollComplete');
                } else {
                    // showLongTop('Highe');
                    $rootScope.loader = false;
                }
            });
        };
        //mainServices.brewerTrends($scope.brewId).then(function(response){
        //    $scope.brewerBrewers = response;
        //});
    })
    .controller('videodetailCtrl', function ($scope, $rootScope, $stateParams, mainServices) {
        $rootScope.backbtn = true;
        $scope.videoId = $stateParams.videoId;
        mainServices.videoDetail($scope.videoId).then(function (response) {
            $scope.videoDetail = response;
        });
        $scope.brewTrendAdd = function(){
            mainServices.brewTrendAdd({videoId:$scope.videoId,brewerId:$rootScope.consumerId}).then(function (response) {
                $scope.responce = response;
            });
        }
    })
    .controller('blogdetailCtrl', function ($scope,$rootScope,mainServices) {
        $rootScope.backbtn = true;
        //console.log($rootScope.blogDetail);
        $scope.brewTrendAdd = function(){
            mainServices.brewTrendAdd({blogId:$rootScope.id,brewerId:$rootScope.consumerId}).then(function (response) {
                $scope.responce = response;
            });
        }
    })
    .controller('AppCtrl', function ($scope, $http, Auth, $rootScope, $ionicPopup) {
        $rootScope.leftMenuItem = '';
        $rootScope.searchProduct = '';
        $rootScope.backbtn = true;
        $rootScope.togglebtn = true;
        $rootScope.loader = false;
        $rootScope.mytrend = false;
        $scope.searchFlag = true;
        $rootScope.searchlayoutFlag = true;


        $rootScope.openExternalUrl = function (url) {
            console.log(url);
            cordova.ThemeableBrowser.open(url, '_blank', {
                toolbar: {
                    height: 64,
                    color: '#444250ff'
                },
                closeButton: {
                    wwwImage: 'img/phase-2/cross-browser.png',
                    imagePressed: 'close_pressed',
                    align: 'right',
                    event: 'closePressed'
                },

                backButtonCanClose: true
            })
        }
        // checking user session
        if (Auth.checkLogin()) {
            var temp = angular.fromJson(Auth.getFields());
            console.log(temp);
            $rootScope.consumerId = temp.consumerId;
            $rootScope.fullName = temp.fullName;
            $rootScope.thumbnail = temp.thumbnail;
            $rootScope.profile = temp.profile;
        } else {
            $rootScope.consumerId = '';
            $rootScope.fullName = '';
            $rootScope.thumbnail = '';
        }
        $scope.logout = function () {
            $rootScope.mytrend = false;
            Auth.logout();
            $rootScope.consumerId = '';
            $rootScope.fullName = '';
            $rootScope.thumbnail = '';

            window.location.href = "#";
        }
        // show search
        $rootScope.showSearch = function () {
            $(".searchBox").slideToggle();
            $scope.searchFlag = !$scope.searchFlag;

        };
        $scope.watsbrew = function () {
            $('.left-menu').find('a').removeClass('active');
            $('.trendBrew').addClass('active');
            $rootScope.leftMenuItem = 'trendBrew';
            $rootScope.mytrend = false;
            window.location.href = "#/app/tab/toptrends";

        };
        $scope.mytrends = function () {
            $('.left-menu').find('a').removeClass('active');
            $('.mytrends').addClass('active');
            $rootScope.leftMenuItem = 'mytrends';
            $rootScope.mytrend = true;
            window.location.href = "#/app/tab/products";

        };
        $rootScope.showErrAlert = function () {
            var alertPopup = $ionicPopup.alert({
                title: 'Error!',
                template: 'Internet Connection Error!'
            });
            alertPopup.then(function (res) {
                //$ionicHistory.goBack();
                //window.location.href = "#/";
            });
        };
    })

    .controller('MytrendsCtrl', function ($rootScope, $scope, $stateParams, $http, swiperPlugin) {
        //For Tabs
        $rootScope.backbtn = false;
        $rootScope.togglebtn = true;
        swiperPlugin.swipein('swiper-container', 3);
        var userId = $rootScope.consumerId;
        $rootScope.loader = true;
        $http.get(serviceURL + 'trends?userId=' + userId).
            success(function (data, status, headers, config) {
                $scope.mytrends = data;
                $rootScope.loader = false;
            });
        idv = 'mytrends1';
        $("#s_" + idv).show();

    })

    .controller('topbrewersCtrl', function ($rootScope, $scope, mainServices) {
        $limit = 0;
        mainServices.topBrewers($limit, $rootScope.consumerId).then(function (response) {
            $scope.topbrewers = response;
        });

    })

    .controller('ToptrendbrewersCtrl', function ($rootScope, $scope, $stateParams, $http, swiperPlugin, Auth) {
        //For Tabs
        $('.left-menu').find('a').removeClass('active');
        $('.toptrendbrewers').addClass('active');
        $rootScope.leftMenuItem = 'toptrendbrewers';
        $rootScope.searchlayoutFlag = true;
        $rootScope.backbtn = false;
        $rootScope.togglebtn = true;
        $scope.isLoggedIn = false;
        $rootScope.filter = true;
        $rootScope.filtercontent = false;
        $scope.predicate = 'numFollowers';
        var userid = '';
        // show filter form
        $scope.showfilteropt = function () {

            $scope.filtercontent = !$scope.filtercontent;
        };
        $scope.filterBytoptrends = function (filterby) {


            $scope.predicate = filterby;
        };
        if ($rootScope.consumerId != undefined && $rootScope.consumerId != '') {
            $scope.isLoggedIn = true;
            userid = '?userId=' + $rootScope.consumerId;
        }
        swiperPlugin.swipein('top-trend-swiper-container', 2);

        $scope.userfollow = function (followerId) {


            if ($rootScope.consumerId != undefined && $rootScope.consumerId != '') {

                var tempobj = {};
                tempobj.userId = $rootScope.consumerId.toString();
                tempobj.followerId = followerId.toString();

                $http.post(serviceURL + 'user/follow', tempobj).
                    success(function (data, status, headers, config) {

                        $.each($scope.topbrewers, function (key, value) {
                            if (value.consumerId == followerId) {
                                $scope.topbrewers[key].isFollower = true;
                            }
                        });
                        // $scope.topbrewers[key].isFollower = true;
                        $rootScope.loader = false;
                    }).error(function (data, status, headers, config) {

                        alert('error');

                    });

            } else {
                alert('no');
            }
        }
        $scope.userunfollow = function (followerId) {

            if ($rootScope.consumerId != undefined && $rootScope.consumerId != '') {

                var tempobj = {};
                tempobj.userId = $rootScope.consumerId.toString();
                tempobj.followerId = followerId.toString();

                $http.post(serviceURL + 'user/unfollow', tempobj).
                    success(function (data, status, headers, config) {

                        $.each($scope.topbrewers, function (key, value) {
                            if (value.consumerId == followerId) {
                                $scope.topbrewers[key].isFollower = false;
                            }
                        });

                        $rootScope.loader = false;
                    }).error(function (data, status, headers, config) {
                        alert('error');
                    });

            } else {
                alert('no');
            }
        }


        //idv = $(".swiper-wrapper").find(".swiper-slide-active").children("div").attr("id");
        idv = 'toptrendbrewer1';
        setTimeout(function () {
            $('.swiper-slide').removeClass('swiper-slide-visible').removeClass('swiper-slide-active');
            $('#toptrendbrewer1').parent().addClass('swiper-slide-visible swiper-slide-active');

        }, 100);
        $("#s_" + idv).show();
        //Getting data from Service
        $rootScope.loader = true;
        $http.get(serviceURL + 'topBrewers' + userid).
            success(function (data, status, headers, config) {
                if (data.status == 200)
                    $scope.topbrewers = data.response;
                else
                    console.log('no data from server');

                $rootScope.loader = false;
            });
        $(".dropdown dt a").click(function () {
            $(".dropdown dd ul").toggle();
        });

        $(".dropdown dd ul li a").click(function () {
            var text = $(this).html();
            $(".dropdown dt a span").html(text);
            $(".dropdown dd ul").hide();

        });


        $(document).bind('click', function (e) {
            var $clicked = $(e.target);
            if (!$clicked.parents().hasClass("dropdown"))
                $(".dropdown dd ul").hide();
        });
    })

    .controller('ShoppingcatalogCtrl', ['$rootScope', '$scope', '$stateParams', '$http', 'swiperPlugin', function ($rootScope, $scope, $stateParams, $http, swiperPlugin) {

        $('.left-menu').find('a').removeClass('active');
        $('.shoppingcatalog').addClass('active');
        $rootScope.leftMenuItem = 'shoppingcatalog';

        $rootScope.backbtn = false;
        $rootScope.togglebtn = true;
        $scope.searchFlag = false;
        $scope.breadcrumb = "Select a category";
        var mytrendUser = "";
        if ($rootScope.mytrend) {
            var userId = '';
            if ($rootScope.consumerId != '' && $rootScope.consumerId != undefined) {
                userId = $rootScope.consumerId;
            }
            mytrendUser = "?userId=" + userId;
        }
        swiperPlugin.swipein('shopping-catalog-swiper-container', 3);
        function loadPage() {

            //	idv = $(".swiper-wrapper").find(".swiper-slide-active").children("div").attr("id");
            $rootScope.loader = true;
            $http.get(serviceURL + 'trends/categories' + mytrendUser).
                success(function (data, status, headers, config) {

                    if (data.status == 200)
                        $scope.categories = data.response;
                    else
                        console.log('error in data');
                    $rootScope.loader = false;
                });
            setTimeout(function () {
                $('.swiper-slide').removeClass('swiper-slide-visible').removeClass('swiper-slide-active');
                $('#sales1').parent().addClass('swiper-slide-visible swiper-slide-active');
            }, 100);

        }

        loadPage();
        //blogproduct()
        jQuery(".blog-info").mouseenter(function () {
            jQuery(this).next(".blog-overlay").show();
        });
        jQuery(".blog-info").mouseleave(function () {
            jQuery(this).next(".blog-overlay").hide();
        });

    }])

    .controller('SettingsCtrl', ['$scope', '$rootScope', '$http', 'socialAuth', 'swiperPlugin', function ($scope, $rootScope, $http, socialAuth, swiperPlugin) {
        $('.left-menu').find('a').removeClass('active');
        $('.settings').addClass('active');
        //For Tabs
        $rootScope.leftMenuItem = 'settings';
        $scope.gender = false;
        $rootScope.backbtn = false;
        $rootScope.togglebtn = true;
        swiperPlugin.swipein('settings-swiper-container', 2);
        $scope.SocialLogin = function (provider) {
            socialAuth.sociallogin(provider);
        }
        $scope.updateProfile = function () {
            var genderLbl = 'm';
            if ($("#gender").prop('checked') == true) {

                genderLbl = 'f';
            } else {
                genderLbl = 'm';
            }
            $rootScope.loader = true;
            $http.post(serviceURL + 'user/update', {
                "consumerId": $rootScope.consumerId,
                "firstName": $scope.firstName,
                "lastName": $scope.lastName,
                "email": $scope.email,
                "gender": genderLbl,
                "country": $scope.country
            }).
                success(function (data, status, headers, config) {

                    if (data.status == 200) {
                        alert('Profile updated');
                    }
                    else
                        console.log('error in data');

                    $rootScope.loader = false;
                });
        }
        function loadPage() {

            $rootScope.loader = true;

            $http.post(serviceURL + 'user/profile', {"consumerId": $rootScope.consumerId}).
                success(function (data, status, headers, config) {

                    if (data.status == 200) {
                        $scope.firstName = data.response.firstName;
                        $scope.lastName = data.response.lastName;
                        $scope.email = data.response.email;
                        $scope.country = data.response.country;
                        if (data.response.gender == 'f')
                            $scope.gender = true;
                        $scope.dob = '';
                    }
                    else
                        console.log('error in data');

                    $rootScope.loader = false;
                });
            setTimeout(function () {
                $('.swiper-slide').removeClass('swiper-slide-visible').removeClass('swiper-slide-active');
                $('#settings1').parent().addClass('swiper-slide-visible swiper-slide-active');
            }, 100);

        }

        loadPage();

    }])

    .controller('settingsstoreCtrl', ['$scope', '$rootScope', '$http', 'socialAuth', 'swiperPlugin', function ($scope, $rootScope, $http, socialAuth, swiperPlugin) {
        $('.left-menu').find('a').removeClass('active');
        $('.settings').addClass('active');
        //For Tabs
        $rootScope.leftMenuItem = 'settings';
        $rootScope.backbtn = false;
        $rootScope.togglebtn = true;
        swiperPlugin.swipein('settings-store-swiper-container', 2);


        $scope.updateStore = function () {

            $rootScope.loader = true;
            $http.post(serviceURL + 'user/store/update', {
                "consumerId": $rootScope.consumerId,
                "id": $scope.id,
                "name": $scope.storename,
                "hashTag": $scope.storehashtag
            }).
                success(function (data, status, headers, config) {

                    if (data.status == 200) {
                        alert('Store updated');
                    }
                    else
                        console.log('error in data');

                    $rootScope.loader = false;
                });
        }
        function loadPage() {
            //$http.post(serviceURL+'user/signup',{"email":$scope.email,"password":$scope.password}).
            //	idv = $(".swiper-wrapper").find(".swiper-slide-active").children("div").attr("id");
            $rootScope.loader = true;
            $http.post(serviceURL + 'user/store', {"consumerId": $rootScope.consumerId}).
                success(function (data, status, headers, config) {

                    if (data.status == 200) {
                        $scope.numProducts = data.response.numProducts;
                        $scope.id = data.response.id;
                        $scope.storename = '';
                        $scope.storehashtag = '';
                        if (data.response.name != undefined)
                            $scope.storename = data.response.name;
                        if (data.response.hashTag != undefined)
                            $scope.storehashtag = data.response.hashTag;
                    }
                    else
                        console.log('error in data');

                    $rootScope.loader = false;
                });
            setTimeout(function () {
                $('.swiper-slide').removeClass('swiper-slide-visible').removeClass('swiper-slide-active');
                $('#settings2').parent().addClass('swiper-slide-visible swiper-slide-active');
            }, 100);

        }

        loadPage();


    }])

    .controller('settingsMerchantsCtrl', ['$scope', '$rootScope', '$http', 'socialAuth', 'swiperPlugin', function ($scope, $rootScope, $http, socialAuth, swiperPlugin) {
        $('.left-menu').find('a').removeClass('active');
        $('.settings').addClass('active');
        //For Tabs
        $rootScope.leftMenuItem = 'settings';
        $rootScope.backbtn = false;
        $rootScope.togglebtn = true;
        $scope.ItemsGridView = true;
        swiperPlugin.swipein('settings-merchants-swiper-container', 2);

        $scope.gridView = function (type) {
            if (type != 'all') {
                $('.merchgridall').removeClass('active');
                $('.merchgridfav').removeClass('active').addClass('active');
                $scope.ItemsGridView = false;

            }
            else {
                $scope.ItemsGridView = true;
                $('.merchgridfav').removeClass('active');
                $('.merchgridall').removeClass('active').addClass('active');
            }
        }
        $scope.addToFav = function (id, key) {

            $rootScope.loader = true;
            $http.post(serviceURL + 'user/addFavorite/merchant', {
                "consumerId": $rootScope.consumerId,
                "merchantId": id
            }).
                success(function (data, status, headers, config) {
                    $.each($scope.favMerchants, function (key, value) {
                        if (value.id == id) {
                            $scope.favMerchants[key].isFavorite = true;
                        }
                    });

                    $rootScope.loader = false;
                });
        }
        $scope.removeToFav = function (id, key) {

            //alert(id);
            // alert('remove');
            $rootScope.loader = true;
            $http.post(serviceURL + 'user/removeFavorite/merchant', {
                "consumerId": $rootScope.consumerId,
                "merchantId": id
            }).
                success(function (data, status, headers, config) {
                    $.each($scope.favMerchants, function (key, value) {
                        if (value.id == id) {
                            $scope.favMerchants[key].isFavorite = false;
                        }
                    });
                    $rootScope.loader = false;
                });
        }
        function loadPage() {
            //$http.post(serviceURL+'user/signup',{"email":$scope.email,"password":$scope.password}).
            //	idv = $(".swiper-wrapper").find(".swiper-slide-active").children("div").attr("id");
            $rootScope.loader = true;
            $http.post(serviceURL + 'provider/favorites', {"consumerId": $rootScope.consumerId}).
                success(function (data, status, headers, config) {


                    if (data.status == 200)
                        $scope.favMerchants = data.response;
                    else
                        console.log('error in data');

                    $rootScope.loader = false;
                });
            setTimeout(function () {
                $('.swiper-slide').removeClass('swiper-slide-visible').removeClass('swiper-slide-active');
                $('#settings3').parent().addClass('swiper-slide-visible swiper-slide-active');
            }, 100);

        }

        loadPage();

        mySwiper.swipeTo(2, 1000, true);

    }])

    .controller('settingsbrandsCtrl', ['$scope', '$rootScope', '$http', 'socialAuth', 'swiperPlugin', function ($scope, $rootScope, $http, socialAuth, swiperPlugin) {
        $('.left-menu').find('a').removeClass('active');
        $('.settings').addClass('active');
        //For Tabs
        $rootScope.leftMenuItem = 'settings';
        $rootScope.backbtn = false;
        $rootScope.togglebtn = true;
        swiperPlugin.swipein('settings-brands-swiper-container', 2);


        $scope.addToFav = function (id, key) {

            $rootScope.loader = true;
            $http.post(serviceURL + 'user/addFavorite/brand', {"consumerId": $rootScope.consumerId, "brandId": id}).
                success(function (data, status, headers, config) {
                    $.each($scope.favBrands, function (key, value) {
                        if (value.id == id) {
                            $scope.favBrands[key].isFavorite = true;
                        }
                    });

                    $rootScope.loader = false;
                });
        }
        $scope.removeToFav = function (id, key) {

            alert(id);
            alert('remove');
            $rootScope.loader = true;
            $http.post(serviceURL + 'user/removeFavorite/brand', {"consumerId": $rootScope.consumerId, "brandId": id}).
                success(function (data, status, headers, config) {
                    $.each($scope.favBrands, function (key, value) {
                        if (value.id == id) {
                            $scope.favBrands[key].isFavorite = false;
                        }
                    });
                    $rootScope.loader = false;
                });
        }
        function loadPage() {

            $rootScope.loader = true;
            $http.post(serviceURL + 'brand/favorites', {"consumerId": $rootScope.consumerId}).
                success(function (data, status, headers, config) {

                    if (data.status == 200)
                        $scope.favBrands = data.response;
                    else
                        console.log('error in data');

                    $rootScope.loader = false;
                });
            setTimeout(function () {
                $('.swiper-slide').removeClass('swiper-slide-visible').removeClass('swiper-slide-active');
                $('#settings4').parent().addClass('swiper-slide-visible swiper-slide-active');
            }, 100);

        }

        loadPage();
        mySwiper.swipeTo(2, 1000, true);

    }])

    .controller('ProductCtrl', ['$scope', '$http', 'Auth', '$rootScope', 'swiperPlugin', function ($scope, $http, Auth, $rootScope, swiperPlugin) {
        $rootScope.backbtn = false;
        $rootScope.togglebtn = true;
        $rootScope.searchlayoutFlag = true;
        var mytrendUser = "";
        if ($rootScope.mytrend) {
            var userId = '';
            if ($rootScope.consumerId != '' && $rootScope.consumerId != undefined) {
                userId = $rootScope.consumerId;
            }
            mytrendUser = "?userId=" + userId;
        }
        $scope.searchFlag = false;
        if ($rootScope.mytrend)
            $scope.breadcrumb = "My Trends";
        else
            $scope.breadcrumb = "Top Trends";
        swiperPlugin.swipein('product-swiper-container', 3);
        function loadPage() {
            //idv = $(".swiper-wrapper").find(".swiper-slide-active").children("div").attr("id");

            //	$("#s_"+idv).show();
            $rootScope.loader = true;

            $http.get(serviceURL + 'trends' + mytrendUser).
                success(function (data, status, headers, config) {
                    if (data.status == 200)
                        $scope.trends = data.response;
                    else
                        console.log('no data from server');
                    $rootScope.loader = false;

                });
            setTimeout(function () {
                $('.swiper-slide').removeClass('swiper-slide-visible').removeClass('swiper-slide-active');
                $('#v1').parent().addClass('swiper-slide-visible swiper-slide-active');

            }, 100);
        }

        loadPage();

    }])

    .controller('myTrendProductCtrl', ['$scope', '$http', 'Auth', '$rootScope', 'swiperPlugin', function ($scope, $http, Auth, $rootScope, swiperPlugin) {

        $rootScope.backbtn = false;
        $rootScope.togglebtn = true;
        $rootScope.searchlayoutFlag = true;
        var mytrendUser = "";
        if ($rootScope.mytrend) {
            var userId = '';
            if ($rootScope.consumerId != '' && $rootScope.consumerId != undefined) {
                userId = $rootScope.consumerId;
            }
            mytrendUser = "?userId=" + userId;
        }
        $scope.searchFlag = false;
        if ($rootScope.mytrend)
            $scope.breadcrumb = "My Trends";
        else
            $scope.breadcrumb = "Top Trends";
        swiperPlugin.swipein('mytrend-product-swiper-container', 3);


        function loadPage() {
            //idv = $(".swiper-wrapper").find(".swiper-slide-active").children("div").attr("id");

            //	$("#s_"+idv).show();
            $rootScope.loader = true;

            $http.get(serviceURL + 'trends' + mytrendUser).
                success(function (data, status, headers, config) {
                    if (data.status == 200)
                        $scope.trends = data.response;
                    else
                        console.log('no data from server');
                    $rootScope.loader = false;

                });
            setTimeout(function () {
                $('.swiper-slide').removeClass('swiper-slide-visible').removeClass('swiper-slide-active');
                $('#v1').parent().addClass('swiper-slide-visible swiper-slide-active');

            }, 100);
        }

        loadPage();

    }])

    .controller('categoriesCtrl', ['$rootScope', '$scope', '$stateParams', '$http', 'swiperPlugin', function ($rootScope, $scope, $stateParams, $http, swiperPlugin) {

        $rootScope.backbtn = false;
        $rootScope.togglebtn = true;
        $scope.searchFlag = false;
        $scope.breadcrumb = "Select a category";
        var mytrendUser = "";
        if ($rootScope.mytrend) {
            var userId = '';
            if ($rootScope.consumerId != '' && $rootScope.consumerId != undefined) {
                userId = $rootScope.consumerId;
            }
            mytrendUser = "?userId=" + userId;
        }
        swiperPlugin.swipein('category-swiper-container', 3);


        function loadPage() {

            //	idv = $(".swiper-wrapper").find(".swiper-slide-active").children("div").attr("id");
            $rootScope.loader = true;
            $http.get(serviceURL + 'trends/categories' + mytrendUser).
                success(function (data, status, headers, config) {
                    if (data.status == 200)
                        $scope.categories = data.response;
                    else
                        console.log('error in data');
                    $rootScope.loader = false;
                });
            setTimeout(function () {
                $('.swiper-slide').removeClass('swiper-slide-visible').removeClass('swiper-slide-active');
                $('#v2').parent().addClass('swiper-slide-visible swiper-slide-active');
            }, 100);

        }

        loadPage();
        $('.swiper-slide').removeClass('swiper-slide-visible').removeClass('swiper-slide-active');
        $('#v2').parent().addClass('swiper-slide-visible swiper-slide-active');
    }])

    .controller('ShoppingbymarchentCtrl', ['$rootScope', '$scope', '$stateParams', '$http', 'swiperPlugin', function ($rootScope, $scope, $stateParams, $http, swiperPlugin) {
        $rootScope.backbtn = false;
        $rootScope.togglebtn = true;
        $scope.searchFlag = false;
        $scope.breadcrumb = "Select a source";
        var mytrendUser = "";
        if ($rootScope.mytrend) {
            var userId = '';
            if ($rootScope.consumerId != '' && $rootScope.consumerId != undefined) {
                userId = $rootScope.consumerId;
            }
            mytrendUser = "?userId=" + userId;
        }
        swiperPlugin.swipein('source-swiper-container', 3);

        function loadPage() {
            //idv = $(".swiper-wrapper").find(".swiper-slide-active").children("div").attr("id");
            $rootScope.loader = true;
            $http.get(serviceURL + 'provider' + mytrendUser).
                success(function (data, status, headers, config) {
                    if (data.status == 200)
                        $scope.merchants = data.response;
                    else
                        console.log('error in data');
                    $rootScope.loader = false;
                });
            setTimeout(function () {
                $('.swiper-slide').removeClass('swiper-slide-visible').removeClass('swiper-slide-active');
                $('#sales3').parent().addClass('swiper-slide-visible swiper-slide-active');
            }, 100);

        }
        loadPage();
    }])

    .controller('sourcesCtrl', ['$rootScope', '$scope', '$stateParams', '$http', 'swiperPlugin', function ($rootScope, $scope, $stateParams, $http, swiperPlugin) {
        $rootScope.backbtn = false;
        $rootScope.togglebtn = true;
        $scope.searchFlag = false;
        $scope.breadcrumb = "Select a source";
        var mytrendUser = "";
        if ($rootScope.mytrend) {
            var userId = '';
            if ($rootScope.consumerId != '' && $rootScope.consumerId != undefined) {
                userId = $rootScope.consumerId;
            }
            mytrendUser = "?userId=" + userId;
        }
        swiperPlugin.swipein('source-swiper-container', 3);


        function loadPage() {

            //idv = $(".swiper-wrapper").find(".swiper-slide-active").children("div").attr("id");
            $rootScope.loader = true;
            $http.get(serviceURL + 'provider' + mytrendUser).
                success(function (data, status, headers, config) {
                    if (data.status == 200)
                        $scope.merchants = data.response;
                    else
                        console.log('error in data');
                    $rootScope.loader = false;
                });
            setTimeout(function () {
                $('.swiper-slide').removeClass('swiper-slide-visible').removeClass('swiper-slide-active');
                $('#v3').parent().addClass('swiper-slide-visible swiper-slide-active');
            }, 100);

        }

        loadPage();


    }])

    .controller('brandsCtrl', ['$rootScope', '$scope', '$stateParams', '$http', 'swiperPlugin', function ($rootScope, $scope, $stateParams, $http, swiperPlugin) {
        $rootScope.backbtn = false;
        $rootScope.togglebtn = true;
        $scope.searchFlag = false;
        $scope.breadcrumb = "Seelect a brand";
        var mytrendUser = "";
        if ($rootScope.mytrend) {
            var userId = '';
            if ($rootScope.consumerId != '' && $rootScope.consumerId != undefined) {
                userId = $rootScope.consumerId;
            }
            mytrendUser = "?userId=" + userId;
        }
        swiperPlugin.swipein('brand-swiper-container', 3);


        function loadPage() {

            //idv = $(".swiper-wrapper").find(".swiper-slide-active").children("div").attr("id");
            $rootScope.loader = true;

            $http.get(serviceURL + 'brands' + mytrendUser).
                success(function (data, status, headers, config) {
                    if (data.status == 200)
                        $scope.brands = data.response;
                    else
                        console.log('error in data');
                    $rootScope.loader = false;
                });
            setTimeout(function () {
                $('.swiper-slide').removeClass('swiper-slide-visible').removeClass('swiper-slide-active');
                $('#v4').parent().addClass('swiper-slide-visible swiper-slide-active');

            }, 100);
        }

        loadPage();
        mySwiper.swipeTo(2, 1000, true);

    }])

    .controller('LoginCtrl', function ($scope, $state, $location, Auth, $rootScope, socialAuth,$http,$cordovaOauth,ngFB) {
        $rootScope.menuToggle = false;
        if (Auth.checkLogin()) {
            var temp = angular.fromJson(Auth.getFields());
            $rootScope.consumerId = temp.consumerId;
            $rootScope.fullName = temp.fullName;
            $rootScope.thumbnail = temp.thumbnail;
            window.location.href = "#/app/tab/toptrends";
        } else {
            $rootScope.consumerId = '';
            $rootScope.fullName = '';
            $rootScope.thumbnail = '';
        }
        $scope.instagramLogin = function () {

            $scope.instagramLogin = function () {
                $cordovaOauth.instagram('04490fb75a354a5199142e64e1fec221',["https://api.instagram.com/oauth/authorize", "http://localhost/callback"])
                    .then(function(result) {
                        console.log(JSON.stringify(result));
                        var endPoint = 'https://api.instagram.com/v1/users/self/?callback=JSON_CALLBACK&access_token=' + result.access_token;
                        $http.jsonp(endPoint).success(function(response) {
                            console.log(response.data);
                            $scope.params = {  };
                            console.log(JSON.stringify($scope.params));
                            $http.post(serviceURL + 'brewer/connect', {
                                "providerId": "instagram",
                                "accessToken":result.access_token
                            })
                                .success(function (data, status, headers, config) {
                                    $rootScope.accessdata = data;
                                    if (data.status == 200) {
                                        var Sesdata = {};
                                        Sesdata.consumerId = data.response.consumerId;
                                        Sesdata.fullName = data.response.fullName;
                                        Sesdata.thumbnail = '';
                                        $rootScope.profile = data.response;
                                        $rootScope.accessdata = data;
                                        Auth.login(Sesdata);
                                        if (Auth.checkLogin()) {
                                            var temp = angular.fromJson(Auth.getFields());
                                            $rootScope.consumerId = temp.consumerId;
                                            $rootScope.fullName = temp.fullName;
                                            $rootScope.thumbnail = temp.thumbnail;
                                            window.location.href = "#/app/tab/toptrends";
                                        }
                                    }
                                    $rootScope.loader = false;
                                })
                                .error(function (data, status, headers, config) {
                                    $scope.error_code = status;
                                    $scope.error_messge = "Error occured while login.";
                                    $rootScope.loader = false;
                                });
                        });
                    }, function(error) {
                        console.log(error);
                    });
            };
        };
        $scope.fbLogin = function () {
            var $fbDetails = {};
            ngFB.login({scope: 'email'}).then(
                function (response) {
                    if (response.status === 'connected') {
                        console.log('Facebook login succeeded');
                        $fbDetails.accessToken = response.authResponse.accessToken;
                        console.log(response.authResponse.accessToken);
                        ngFB.api({
                            path: '/me',
                            params: {fields: 'id,name'}
                        }).then(
                            function (user) {
                                $fbDetails.userId = user.id;
                                //$fbDetails.screenName = user.name;
                                $fbDetails.providerid = "facebook";
                                $fbDetails.accessToken = response.authResponse.accessToken;
                                $scope.user = $fbDetails;
                                $rootScope.loader = true;
                                $http.post(serviceURL + 'brewer/connect', {
                                    "providerId": "facebook",
                                    "accessToken":response.authResponse.accessToken
                                })
                                    .success(function (data, status, headers, config) {
                                        $rootScope.accessdata = data;
                                        if (data.status == 200) {
                                            var Sesdata = {};
                                            Sesdata.consumerId = data.response.consumerId;
                                            Sesdata.fullName = data.response.fullName;
                                            Sesdata.thumbnail = '';
                                            $rootScope.profile = data.response;
                                            $rootScope.accessdata = data;
                                            Auth.login(Sesdata);
                                            if (Auth.checkLogin()) {
                                                var temp = angular.fromJson(Auth.getFields());
                                                $rootScope.consumerId = temp.consumerId;
                                                $rootScope.fullName = temp.fullName;
                                                $rootScope.thumbnail = temp.thumbnail;
                                                window.location.href = "#/app/tab/toptrends";
                                            }
                                        }
                                        $rootScope.loader = false;
                                    })
                                    .error(function (data, status, headers, config) {
                                        $scope.error_code = status;
                                        $scope.error_messge = "Error occured while login.";
                                        $rootScope.loader = false;
                                    });

                            },
                            function (error) {
                                alert('Facebook error: ' + error.error_description);
                            });
                        console.log(response);
                        // alert('Facebook logined');
                        //$scope.closeLogin();
                    } else {
                        alert('Facebook login failed');
                    }
                });
        };

        $('.left-menu').find('a').removeClass('active');
        $('.loginmenu').addClass('active');
        $rootScope.backbtn = false;
        $rootScope.togglebtn = true;
        $scope.normalLgoin = false;
        $scope.signupFlag = false;
        $rootScope.searchlayoutFlag = false;

        if ($rootScope.error_code == 500) {
            $scope.err_code = $rootScope.error_code;
            $scope.err_msg = $rootScope.error_message;
            $rootScope.error_code = '';
            $rootScope.error_message = '';
        }
        $scope.SocialLogin = function (provider) {
            socialAuth.sociallogin(provider);
        }
        $scope.showproducts = function () {
            $('.left-menu').find('a').removeClass('active');
            $('.trendBrew').addClass('active');
            $rootScope.leftMenuItem = 'trendBrew';
            $rootScope.mytrend = false;
            $state.transitionTo('app.tabs.toptrends');
        };
        $scope.logStore = function (data) {

            var data = {};
            data.uid = 12345;
            data.username = 'surendra'
            Auth.login(data);
            if (Auth.checkLogin()) {
                var temp = angular.fromJson(Auth.getFields());


                $rootScope.uid = temp.uid;
                $rootScope.username = temp.username;

            }
            window.location.href = "#/app/products";

        };


        // show hide normal login content
        $scope.showNormalLoginBlcok = function () {

            $scope.normalLgoin = !$scope.normalLgoin;
        };
        // show signup form
        $scope.ShowSignup = function () {

            $scope.signupFlag = !$scope.signupFlag;
        };
        // show signin form
        $scope.ShowSignin = function () {

            $scope.signupFlag = !$scope.signupFlag;
        };

    })

    .controller('ProductdetailsCtrl', ['$scope', '$http', 'Auth', '$rootScope', function ($scope, $http, Auth, $rootScope) {

        $rootScope.backbtn = false;
        $rootScope.togglebtn = true;
        $rootScope.searchlayoutFlag = true;
        var mytrendUser = "";
        if ($rootScope.mytrend) {
            var userId = '';
            if ($rootScope.consumerId != '' && $rootScope.consumerId != undefined) {
                userId = $rootScope.consumerId;
            }
            mytrendUser = "?userId=" + userId;
        }
        $scope.searchFlag = false;
        if ($rootScope.mytrend)
            $scope.breadcrumb = "My Trends";
        else
            $scope.breadcrumb = "Top Trends";

        function loadPage() {
            //idv = $(".swiper-wrapper").find(".swiper-slide-active").children("div").attr("id");

            //	$("#s_"+idv).show();
            $rootScope.loader = true;

            $http.get(serviceURL + 'trends' + mytrendUser).
                success(function (data, status, headers, config) {
                    if (data.status == 200)
                        $scope.trends = data.response;
                    else
                        console.log('no data from server');
                    $rootScope.loader = false;

                });
        }

        loadPage();

    }])

    .controller('profileCtrl', ['$rootScope', '$scope', '$stateParams', '$http', 'swiperPlugin', '$ionicPopover', '$ionicModal', '$state', function ($rootScope, $scope, $stateParams, $http, swiperPlugin, $ionicPopover, $ionicModal, $state) {
        $('.left-menu').find('a').removeClass('active');
        $('.profile').addClass('active');
        $ionicPopover.fromTemplateUrl('templates/profilepopover.html', {
            scope: $scope
        }).then(function (popover) {
            $scope.popover = popover;
        });
        $scope.openPopover = function ($event) {
            $scope.popover.show($event);
        };
        $scope.stateGoTo = function (stateTo) {
            $scope.popover.hide();
            $state.transitionTo(stateTo);
        }


        $ionicModal.fromTemplateUrl('popupproductdetails.html', function ($ionicModal) {
            $scope.modal = $ionicModal;
        }, {
            // Use our scope for the scope of the modal to keep it simple
            scope: $scope,
            // The animation we want to use for the modal entrance
            animation: 'slide-in-up'
        });
        $scope.showConfirm = function () {
            $scope.modal.show();
        }
        //working
        $rootScope.backbtn = true;
        $rootScope.togglebtn = false;
        var sourceName = $stateParams.sourceName;
        var brandId = $stateParams.brandId;
        var sourceId = $stateParams.sourceId;
        var mytrendUser = '';
        if ($rootScope.mytrend) {
            var userId = '';
            if ($rootScope.consumerId != '' && $rootScope.consumerId != undefined) {
                userId = $rootScope.consumerId;
            }
            mytrendUser = "&userId=" + userId;
        }
        swiperPlugin.swipein('brand-source-items-swiper-container', 2);

        //$rootScope.loader=true;

        $http.get(serviceURL + 'trends?brand=' + brandId + '&provider=' + sourceId + mytrendUser).
            success(function (data, status, headers, config) {
                if (data.status == 200) {
                    $scope.brandsourceitems = data.response;
                }
                else {
                    console.log('no data from db');
                }

                $rootScope.loader = false;

            });

        $scope.sourceName = sourceName;
        $scope.brandId = brandId;
        $scope.sourceId = sourceId;
        setTimeout(function () {
            $('.swiper-slide').removeClass('swiper-slide-visible').removeClass('swiper-slide-active');
            $('#v1').parent().addClass('swiper-slide-visible swiper-slide-active');
        }, 100);

    }])

    .controller('storeCtrl', ['$rootScope', '$scope', 'swiperPlugin', '$ionicPopover', '$ionicModal', '$state', function ($rootScope, $scope, swiperPlugin, $ionicPopover, $ionicModal, $state) {
        $ionicPopover.fromTemplateUrl('templates/profilepopover.html', {
            scope: $scope
        }).then(function (popover) {
            $scope.popover = popover;
        });
        $scope.openPopover = function ($event) {
            $scope.popover.show($event);
        };
        $scope.stateGoTo = function (stateTo) {
            $scope.popover.hide();
            $state.transitionTo(stateTo);
        }
        $ionicModal.fromTemplateUrl('popupproductdetails.html', function ($ionicModal) {
            $scope.modal = $ionicModal;
        }, {
            // Use our scope for the scope of the modal to keep it simple
            scope: $scope,
            // The animation we want to use for the modal entrance
            animation: 'slide-in-up'
        });
        $scope.showConfirm = function () {
            $scope.modal.show();
        }
    }])

    .controller('trendbrewersdetailsCtrl', function ($rootScope, $scope, $stateParams, profile) {
        console.log($stateParams.brewerId);
        $scope.brewerId = $stateParams.brewerId;

        $scope.brewerDetails = function ($brewerId) {
            //if($brewerId == undefined)
            //    $brewerId = $scope.brewerId;
            $rootScope.loader = true;
            profile.getBrewerDetails($brewerId, $rootScope.consumerId).then(function (data) {
                console.log(data);
                $scope.brewDetails = data;
                $rootScope.loader = false;
            });
        }
        if ($scope.brewerId != undefined) {
            $scope.brewerDetails($scope.brewerId);
        }
        $scope.follow = function (brewId) {
            $scope.isFollower = true;
            $rootScope.loader = true;
            profile.follow(brewId, $rootScope.consumerId).then(function (data) {
                $rootScope.loader = false;
                $scope.brewerDetails(brewId);
            });
        }
        $scope.unFollow = function (brewId) {
            $scope.isFollower = false;
            $rootScope.loader = true;
            profile.unFollow(brewId, $rootScope.consumerId).then(function (data) {
                $rootScope.loader = false;
                $scope.brewerDetails(brewId);
            });
        }
    })

    .controller('videosCtrl', function ($rootScope, $scope, swiperPlugin, $ionicPopover, $ionicModal, $state, $stateParams, profile) {
        $rootScope.backbtn = true;
        $scope.brewId = $stateParams.brewId;
        $scope.openPopover = function ($event) {
            $scope.popover.show($event);
        };
        $scope.stateGoTo = function (stateTo) {
            $scope.popover.hide();
            $state.transitionTo(stateTo);
        }
        $ionicModal.fromTemplateUrl('popupproductdetails.html', function ($ionicModal) {
            $scope.modal = $ionicModal;
        }, {
            // Use our scope for the scope of the modal to keep it simple
            scope: $scope,
            // The animation we want to use for the modal entrance
            animation: 'slide-in-up'
        });
        $scope.showConfirm = function () {
            $scope.modal.show();
        }

        $scope.addVideo = function (videoData) {
            profile.addVideo($rootScope.consumerId, videoData);
            $scope.videoData = {};
            $state.go('app.tabs.videos', {"brewId": $scope.brewId});

        }

        function loadPage() {
            $rootScope.loader = true;
            profile.brewerVideos($scope.brewId).then(function (response) {
                $scope.brewerVideos = response;
            });
        }

        loadPage();
    })

    .controller('productVideosCtrl', function ($rootScope, $scope, mainServices, $state, $stateParams, profile) {
        $rootScope.backbtn = true;
        $scope.productId = $stateParams.productId;
        $scope.addVideo = function (videoData) {
            mainServices.addVideo($scope.productId, videoData);
            $scope.videoData = {};
            $state.go('app.tabs.productvideos', {"productId": $scope.productId});
        }

        function loadPage() {
            $rootScope.loader = true;
            mainServices.productVideos($scope.productId).then(function (response) {
                $scope.productVideos = response;
            });
        }

        loadPage();
    })

    .controller('productBlogsCtrl', function ($rootScope, $scope, $state, profile, mainServices, $stateParams) {
        $rootScope.backbtn = true;
        $rootScope.togglebtn = true;
        $scope.searchFlag = false;
        $scope.productId = $stateParams.productId;
        $scope.blogDetails = function (bolgData) {
            $rootScope.blogDetail = bolgData;
        }

        $scope.addBlog = function (blogData) {
            mainServices.addBlog($scope.productId, blogData);
            $state.go('app.tabs.productblogs', {"productId": $scope.productId});
        }
        mainServices.productBlogs($scope.productId).then(function (response) {
            $scope.productBlogs = response;
        });
    })

    .controller('aboutCtrl', function ($rootScope, $scope, swiperPlugin, $ionicPopover, $ionicModal, $state, profile) {
        profile.get($rootScope.consumerId).then(function (data) {
            $rootScope.profile = data;
        });
        $scope.saveProfile = function ($data) {
            console.log($data);
            profile.profileUpdate($data);
        }

    })

    .controller('socialCtrl', ['$rootScope', '$scope', 'swiperPlugin', '$ionicPopover', '$ionicModal', '$state', function ($rootScope, $scope, swiperPlugin, $ionicPopover, $ionicModal, $state) {
        $ionicPopover.fromTemplateUrl('templates/profilepopover.html', {
            scope: $scope
        }).then(function (popover) {
            $scope.popover = popover;
        });
        $scope.openPopover = function ($event) {
            $scope.popover.show($event);
        };
        $scope.stateGoTo = function (stateTo) {
            $scope.popover.hide();
            $state.transitionTo(stateTo);
        }
        $ionicModal.fromTemplateUrl('popupproductdetails.html', function ($ionicModal) {
            $scope.modal = $ionicModal;
        }, {
            // Use our scope for the scope of the modal to keep it simple
            scope: $scope,
            // The animation we want to use for the modal entrance
            animation: 'slide-in-up'
        });
        $scope.showConfirm = function () {
            $scope.modal.show();
        }
    }])

    .controller('signupCtrl', function ($rootScope, $scope, $state, $location, $http, Auth) {
        $rootScope.searchlayoutFlag = false;
        $rootScope.menuToggle = false;
        $scope.submitted = false;
        $scope.ShowSignin = function () {
            $scope.error_messge = '';
            $state.go('app.tabs.signin');
        };
        $scope.signup = function (form, user) {

            $scope.submitted = true;
            if (form.$valid) {
                $rootScope.loader = true;
                $http.post(serviceURL + 'brewer/signup', {
                    "firstName": user.fname,
                    "lastName": user.lname,
                    "email": user.email,
                    "password": user.password,
                    "gender": user.gender
                }).
                    success(function (data, status, headers, config) {

                        $rootScope.accessdata = data;
                        if (data.status == 200) {
                            var Sesdata = {};
                            Sesdata.consumerId = data.response.consumerId;
                            Sesdata.fullName = data.response.fullName;
                            Sesdata.thumbnail = '';
                            $rootScope.profile = data.response;
                            /* profile.get(data.response.consumerId).then(function (data) {
                             $rootScope.profile = data;
                             });
                             */
                            $rootScope.accessdata = data;
                            Auth.login(Sesdata);
                            if (Auth.checkLogin()) {
                                var temp = angular.fromJson(Auth.getFields());
                                $rootScope.consumerId = temp.consumerId;
                                $rootScope.fullName = temp.fullName;
                                $rootScope.thumbnail = temp.thumbnail;
                            }
                            //hello(r.network).api('me').on('complete', function(r){
                            //          window.location.href = "#/app/products";
                            //});
                            window.location.href = "#/app/products";
                        }
                        $rootScope.loader = false;
                    }).
                    error(function (data, status, headers, config) {
                        $scope.error_code = status;
                        $scope.error_messge = "Error occured while login.";
                        $rootScope.loader = false;
                    });
            }
            ;
        }
    })

    .controller('signinCtrl', function ($rootScope, $scope, $state, $location, $http, Auth, profile) {
        $rootScope.searchlayoutFlag = false;
        $rootScope.menuToggle = false;
        $scope.submitted = false;
        $scope.ShowSignup = function () {
            $scope.error_messge = '';
            $state.go('app.tabs.signup');
        };

        $scope.signin = function (form, user) {
            $scope.submitted = true;
            if (form.$valid) {
                $rootScope.loader = true;
                $http.post(serviceURL + 'brewer/login', {"username": user.email, "password": user.password}).
                    success(function (data, status, headers, config) {
                        $rootScope.loader = false;
                        $rootScope.accessdata = data;
                        if (data.status == 200) {
                            $rootScope.profile = data.response;
                            var Sesdata = {};
                            Sesdata.consumerId = $rootScope.profile.consumerId;//data.response.consumerId;
                            Sesdata.fullName = $rootScope.profile.firstName;//data.response.fullName;
                            Sesdata.thumbnail = $rootScope.profile.imageURL;
                            Sesdata.profile = $rootScope.profile;
                            $rootScope.accessdata = data;
                            Auth.login(Sesdata);
                            if (Auth.checkLogin()) {

                                var temp = angular.fromJson(Auth.getFields());
                                $rootScope.consumerId = temp.consumerId;
                                $rootScope.fullName = temp.fullName;
                                $rootScope.thumbnail = temp.thumbnail;
                            }
                            //hello(r.network).api('me').on('complete', function(r){
                            //          window.location.href = "#/app/products";
                            //});
                            window.location.href = "#/app/tab/toptrends";

                        } else {
                            $scope.error_messge = data.response;
                        }
                        //$rootScope.loader = false;
                    }).
                    error(function (data, status, headers, config) {
                        $scope.error_code = status;
                        $scope.error_messge = "Invalid email or password.";

                        $rootScope.loader = false;


                    });
            }
            ;
        }
    })

    .controller('brewsFiltersCtrl', function ($rootScope, $scope, mainServices,$ionicModal) {
        $scope.searchKey = {value: ''};
        $limitFrom = 1;
        $scope.showExternalUrls = false;

        $scope.closeSearchResult = function () {
            $scope.showExternalUrls = false
        }
        $scope.searchChange = function ($key) {
            console.log($key.length);
            $scope.showExternalUrls = true;
            $scope.externalUrls = [];
            search($key, $limitFrom);
        }

        $scope.searchWord = function (word) {
            $scope.showExternalUrls = true;
            $scope.searchKey.value = word;
            $scope.externalUrls = [];
            search(word, $limitFrom);
        };
        function search($word, $limitFrom) {
            mainServices.shopSearch($word, $limitFrom).then(function (data) {
                console.log(data);
                $scope.externalUrls = data.response;
                //  $rootScope.loader = false;
            });
        }
        $ionicModal.fromTemplateUrl('bookmark-modal.html', function ($ionicModal) {
            $scope.modal = $ionicModal;
        },{scope: $scope,animation: 'slide-in-up'});
        $scope.url = '';
        $scope.goToExternalUrl = function (url) {
            $scope.showConfirm();
            return false;
            var externalBrowser = cordova.ThemeableBrowser.open('http://' + url, '_blank', {
                statusbar: {
                    color: '#ffffffff'
                },
                toolbar: {
                    height: 64,
                    color: '#444250ff'
                },
                closeButton: {
                    wwwImage: 'img/phase-2/cross-browser.png',
                    imagePressed: 'close_pressed',
                    align: 'right',
                    event: 'closePressed'
                },
                customButtons: [
                    {
                        wwwImage: 'img/phase-2/bookmark.png',
                        imagePressed: 'share_pressed',
                        align: 'left',
                        event: 'sharePressed'
                    }
                ],
                backButtonCanClose: true
            });
            externalBrowser.addEventListener('sharePressed', function (e) {
                $scope.showConfirm();
                $scope.addToBookmark(e.url);
                externalBrowser.close();
            });
        }
        $scope.showConfirm = function () {
            $scope.modal.show();
        }


        $scope.addToBookmark = function(url){
            mainServices.addToBookmark(url).then(function (response) {
                $scope.productBlogs = response;
            });
        }
    })

    .controller('stylesCtrl', ['$scope', '$rootScope', 'swiperPlugin', '$ionicSlideBoxDelegate', '$ionicScrollDelegate', function ($scope, $rootScope, swiperPlugin, $ionicSlideBoxDelegate, $ionicScrollDelegate) {
        $rootScope.backbtn = true;
        $scope.slideIndex = 1;
        $scope.nextSlide4 = function () {
            if ($scope.slideIndex < 2) {
                $scope.slideIndex++;
            }
        }
        $scope.previousSlide4 = function () {
            if ($scope.slideIndex > 1) {
                $scope.slideIndex--;
            }
        }
    }])
















