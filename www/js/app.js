var serviceURL = "http://66.28.216.130:8080/ws/";
var siteUrl = "http://trendbrew.com/"
var UrlPath = '/myapp/www/';
var fbUserId = '';
var myApp = angular.module('starter', ['ionic', 'starter.services', 'starter.controllers', 'ionicLazyLoad', 'ngOpenFB','ngCordova'])
    .run(function ($ionicPlatform, $rootScope, $location, $state, Auth, ngFB) {
        ngFB.init({appId: '1854746061417624'});//511501315676126  @@@  1854746061417624
        $ionicPlatform.ready(function () {
            //  navigator.splashscreen.hide();
            if (window.cordova && window.cordova.plugins.Keyboard) {
                cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
            }
            if (window.StatusBar) {
                // org.apache.cordova.statusbar required
                StatusBar.styleDefault();
            }
        });
        var history = [];
        $rootScope.dragable = true;
        $rootScope.rootParam = true;
        $rootScope.limitCount = 20;
        $rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams) {
            $rootScope.loader = false;
            $rootScope.menuToggle = true;
            $rootScope.footerVisible = true;
            //console.log("event",event, 'toState',toState, toParams, fromState, fromParams);
            if (toState.name == 'app.tabs.login' || toState.name == 'app.tabs.signin' || toState.name == 'app.tabs.signup') {
                $rootScope.footerVisible = false;
                $rootScope.dragable = false;
            } else {
                $rootScope.footerVisible = true;
                $rootScope.dragable = true;
            }
            if (!Auth.checkLogin() && $rootScope.rootParam) {
                $rootScope.rootParam = false;
                $state.transitionTo('app.tabs.login');
            }
        });
        $rootScope.$on('$stateChangeSuccess', function ($rootScope) {
            history.push($location.$$path);
        });

        $rootScope.back = function () {
            var prevUrl = history.length > 1 ? history.splice(-2)[0] : "/";
            $location.path(prevUrl);
        };
        $rootScope.mySelectionItem = [];

    })

    .config(function ($stateProvider, $urlRouterProvider) {

        $stateProvider
            .state('app', {
                url: "/app",
                templateUrl: "menu.html",
                controller: 'AppCtrl'
            })

            .state('app.tabs', {
                url: "/tab",

                views: {
                    'menu-content': {
                        templateUrl: "tabs.html",
                    }
                }
            })
            .state('app.tabs.toptrends', {
                url: "/toptrends?cid&pid&rid&bid&sortType&q",
                views: {
                    'toptrends-tab': {
                        templateUrl: "templates/toptrends.html",
                        controller: 'toptrendsCtrl'
                    }
                }
            })
            .state('app.tabs.login', {
                url: "/login",
                views: {
                    'toptrends-tab': {
                        templateUrl: "templates/login.html",
                        controller: 'LoginCtrl'
                    }
                }
            })
            .state('app.tabs.signup', {
                url: "/signup",
                views: {
                    'toptrends-tab': {
                        templateUrl: "templates/signup.html",
                        controller: 'signupCtrl'
                    }
                }
            })
            .state('app.tabs.signin', {
                url: "/signin",
                views: {
                    'toptrends-tab': {
                        templateUrl: "templates/signin.html",
                        controller: 'signinCtrl'
                    }
                }
            })
            .state('app.tabs.topbrewers', {
                url: "/topbrewers",
                views: {
                    'toptrends-tab': {
                        templateUrl: "templates/topbrewers.html",
                        controller: 'topbrewersCtrl'
                    }
                }
            })

            .state('app.tabs.topvideos', {
                url: "/topvideos?sortType",
                views: {
                    'toptrends-tab': {
                        templateUrl: "templates/topvideos.html",
                        controller: 'topVideosCtrl'
                    }
                }
            })

            .state('app.tabs.topblogs', {
                url: "/topblogs",
                views: {
                    'toptrends-tab': {
                        templateUrl: "templates/topblogs.html",
                        controller: 'topBlogsCtrl'
                    }
                }
            })
            .state('app.tabs.productviewdetails', {
                url: "/productviewdetails/:productId",
                views: {
                    'toptrends-tab': {
                        templateUrl: "templates/productdetails.html",
                        controller: 'productviewdetailsCtrl'
                    }
                }
            })
            .state('app.tabs.trendbrewersdetails', {
                url: "/trendbrewersdetails/:brewerId",
                views: {
                    'toptrends-tab': {
                        templateUrl: "templates/trendbrewersdetails.html",
                        controller: 'trendbrewersdetailsCtrl'
                    }
                }
            })
            .state('app.tabs.mytrendstore', {
                url: "/mytrendstore/:brewId",
                views: {
                    'toptrends-tab': {
                        templateUrl: "templates/mytrendstore.html",
                        controller: 'mytrendstoreCtrl'
                    }
                }
            })
            .state('app.tabs.popupproductdetails', {
                url: "/popupproductdetails",
                views: {
                    'toptrends-tab': {
                        templateUrl: "popupproductdetails.html",
                        controller: 'popupproductdetailsCtrl'
                    }
                }
            })
            .state('app.tabs.brewfeed', {
                url: "/brewfeed/:brewId",
                views: {
                    'toptrends-tab': {
                        templateUrl: "templates/brewfeed.html",
                        controller: 'brewfeedCtrl'
                    }
                }
            })
            .state('app.tabs.store', {
                url: "/store/:brewId",
                views: {
                    'toptrends-tab': {
                        templateUrl: "templates/mystore.html",
                        controller: 'mystoreCtrl'
                    }
                }
            })
            .state('app.tabs.myoffers', {
                url: "/myoffers/:brewId",
                views: {
                    'toptrends-tab': {
                        templateUrl: "templates/myoffers.html",
                        controller: 'myoffersCtrl'
                    }
                }
            })
            .state('app.tabs.mywishes', {
                url: "/mywishes/:brewId?cid&pid&rid&bid&sortType",
                views: {
                    'toptrends-tab': {
                        templateUrl: "templates/mywishes.html",
                        controller: 'mywishesCtrl'
                    }
                }
            })
            .state('app.tabs.followers', {
                url: "/followers/:brewId",
                views: {
                    'toptrends-tab': {
                        templateUrl: "templates/followers.html",
                        controller: 'followersCtrl'
                    }
                }
            })
            .state('app.tabs.following', {
                url: "/following/:brewId",
                views: {
                    'toptrends-tab': {
                        templateUrl: "templates/following.html",
                        controller: 'followingCtrl'
                    }
                }
            })
            .state('app.tabs.videos', {
                url: "/videos/:brewId",
                views: {
                    'toptrends-tab': {
                        templateUrl: "templates/brewers-videos.html",
                        controller: 'videosCtrl'
                    }
                }
            })
            .state('app.tabs.blogs', {
                url: "/blogs/:brewId",
                views: {
                    'toptrends-tab': {
                        templateUrl: "templates/brewer-blogs.html",
                        controller: 'myBlogsCtrl'
                    }
                }
            })
            .state('app.tabs.about', {
                url: "/about",
                views: {
                    'toptrends-tab': {
                        templateUrl: "templates/about.html",
                        controller: 'aboutCtrl'
                    }
                }
            })
            .state('app.tabs.brewpoints', {
                url: "/brewpoints/:brewId",
                views: {
                    'toptrends-tab': {
                        templateUrl: "templates/mybrewpoints.html",
                        controller: 'mybrewpointsCtrl'
                    }
                }
            })
            .state('app.tabs.productvideos', {
                url: "/productvideos/:productId",
                views: {
                    'toptrends-tab': {
                        templateUrl: "templates/productvideos.html",
                        controller: 'productVideosCtrl'
                    }
                }
            })
            .state('app.tabs.productblogs', {
                url: "/productblogs/:productId",
                views: {
                    'toptrends-tab': {
                        templateUrl: "templates/productblogs.html",
                        controller: 'productBlogsCtrl'
                    }
                }
            })
            .state('app.tabs.styles', {
                url: "/styles",
                views: {
                    'toptrends-tab': {
                        templateUrl: "templates/style.html",
                        controller: 'stylesCtrl'
                    }
                }
            })
            .state('app.tabs.brews', {
                url: "/brews/:brewId?cid&pid&rid&bid&sortType",
                views: {
                    'toptrends-tab': {
                        templateUrl: "templates/brews.html",
                        controller: 'brewsCtrl'
                    }
                }
            })
            .state('app.tabs.storebrews', {
                url: "/storebrews/:brewId",
                views: {
                    'toptrends-tab': {
                        templateUrl: "templates/store-brews.html",
                        controller: 'storeBrewsCtrl'
                    }
                }
            })
            .state('app.tabs.videodetail', {
                url: "/videodetail/:videoId",
                views: {
                    'toptrends-tab': {
                        templateUrl: "templates/video-detail.html",
                        controller: 'videodetailCtrl'
                    }
                }
            })
            .state('app.tabs.brewersProfile', {
                url: "/brewersProfile/:brewId",
                views: {
                    'toptrends-tab': {
                        templateUrl: "templates/brewersProfile.html",
                        controller: 'brewersProfileCtrl'
                    }
                }
            })
            .state('app.tabs.recentlyadded', {
                url: "/recentlyadded?sortType",
                views: {
                    'toptrends-tab': {
                        templateUrl: "templates/recently-added.html",
                        controller: 'topVideosCtrl'
                    }
                }
            })
            .state('app.tabs.myvideos', {
                url: "/myvideos/:brewId",
                views: {
                    'toptrends-tab': {
                        templateUrl: "templates/myvideos.html",
                        controller: 'videosCtrl'
                    }
                }
            })
            .state('app.tabs.blogdetail', {
                url: "/blogdetail",
                views: {
                    'toptrends-tab': {
                        templateUrl: "templates/blog-detail.html",
                        controller: 'blogdetailCtrl'
                    }
                }
            })
            .state('app.tabs.recentlyaddedblogs', {
                url: "/recentlyaddedblogs?sortType",
                views: {
                    'toptrends-tab': {
                        templateUrl: "templates/recently-added-blogs.html",
                        controller: 'topBlogsCtrl'
                    }
                }
            })
            .state('app.tabs.myblogs', {
                url: "/myblogs/:brewId",
                views: {
                    'toptrends-tab': {
                        templateUrl: "templates/myblogs.html",
                        controller: 'myBlogsCtrl'
                    }
                }
            })
            .state('app.tabs.brewsfilters', {
                url: "/shop",
                views: {
                    'toptrends-tab': {
                        templateUrl: "templates/brewsfilters.html",
                        controller: 'brewsFiltersCtrl'
                    }
                }
            })
        // if none of the above states are matched, use this as the fallback
        $urlRouterProvider.otherwise('/app/tab/login');
    })
/*.controller('popupproductdetailsCtrl',  ['$rootScope','$scope','$stateParams','$http','swiperPlugin','$ionicPopup', '$timeout',function( $rootScope,$scope,$stateParams , $http,swiperPlugin,$ionicPopup,$timeout) {
 // Triggered on a button click, or some other target
 $scope.showConfirm = function(text) {
 alert(text);
 }
 $scope.showPopup = function() {
 $scope.data = {}

 // A confirm dialog
 $scope.showConfirm = function() {

 var confirmPopup = $ionicPopup.confirm({
 title: 'Consume Ice Cream',
 template: 'Are you sure you want to eat this ice cream?'
 });
 confirmPopup.then(function(res) {
 if(res) {
 console.log('You are sure');
 } else {
 console.log('You are not sure');
 }
 });
 };
 };
 }]);*/


// limit to the string module
angular.module('starter').
    filter('truncate', function () {
        return function (text, length, end) {
            if (isNaN(length))
                length = 10;

            if (end === undefined)
                end = "...";

            if (text.length <= length) {
                return text;
            }
            else {
                return String(text).substring(0, length - end.length) + end;
            }

        };
    });

angular.module('starter').
    filter('numberFormat', function () {
        return function (num) {
            if (num >= 1000000000) {
                return (num / 1000000000).toFixed(1).replace(/\.0$/, '') + 'B';
            }
            if (num >= 1000000) {
                return (num / 1000000).toFixed(1).replace(/\.0$/, '') + 'M';
            }
            if (num >= 1000) {
                return (num / 1000).toFixed(1).replace(/\.0$/, '') + 'K';
            }
            return num;

        };
    });

// number foramts

angular.module('starter').
    filter('dateformat', function () {
        return function (d) {

            var monthNames = [
                "Jan", "Feb", "Mar",
                "Apr", "May", "Jun", "Jul",
                "Aug", "Sep", "Oct",
                "Nov", "Dec"
            ];

            var date = new Date(d);
            var day = date.getDate();
            var monthIndex = date.getMonth();
            var year = date.getFullYear();
            var formt = monthNames[monthIndex] + ' ' + day + ', ' + year;
            return formt;

        };
    });

//stroing data in localstorage
myApp.factory('Auth', function ($rootScope) {
    return {
        checkLogin: function () {
            // Check if logged in and fire events
            if (this.isLoggedIn()) {
                $rootScope.$broadcast('app.loggedIn');
                return true;
            } else {
                $rootScope.$broadcast('app.loggedOut');
                return false;
            }
        },
        isLoggedIn: function () {
            if (window.localStorage['app'] != undefined)
                return true;
            return false;
            // Check auth token here from localStorage
        },
        getFields: function () {
            if (window.localStorage['app'] != undefined)
                return window.localStorage['app'];
        },
        login: function (data) {
            window.localStorage['app'] = angular.toJson(data);

            //window.localStorage['app'].id = data.id;
            //window.localStorage['app'].name = data.name;
            //window.localStorage['app'].spouce = data.spouce;

            //  $rootScope.$broadcast('app.loggedIn'); 
        },
        logout: function (user, pass) {

            window.localStorage.clear();
            $rootScope.$broadcast('app.loggedOut');
        }
    }
});
//Social logins auth
myApp.factory('socialAuth', ['$rootScope', 'Auth', '$http', function ($rootScope, Auth, $http) {
    return {
        sociallogout: function (provider) {

            hello("facebook").logout().then(function () {

                //alert("Signed out");
            }, function (e) {
                //alert( "Signed out error:" + e.error.message );
            });


        },
        sociallogin: function (provider) {

            hello(provider).login(this.loginHandler);
        },
        loginHandler: function (r) {

            var netw = r.network;
            $rootScope.accessdata = hello.getAuthResponse(r.network);
            if (r.network == 'google')
                netw = 'googleplus';

            var jsonUser = {"accessToken": r.authResponse.access_token, "providerId": netw};
            if (r.network == 'twitter') {

                jsonUser = {
                    "accessToken": r.authResponse.access_token,
                    "providerId": netw,
                    "accessSecret": "PMud1irLKZZkY6ZE9141S9OB99xOMRiDafRDftkR9so",
                    "screenName": r.authResponse.screen_name
                };
            }

            $rootScope.accessdata = r;

            $rootScope.loader = true;
            $http.post(serviceURL + 'user/add', jsonUser).
                success(function (data, status, headers, config) {

                    $rootScope.accessdata = data;
                    if (data.status == 200) {

                        var Sesdata = {};
                        Sesdata.consumerId = data.response.consumerId;
                        Sesdata.fullName = data.response.fullName;
                        Sesdata.thumbnail = data.response.imageURL;
                        $rootScope.accessdata = data;
                        Auth.login(Sesdata);
                        if (Auth.checkLogin()) {

                            var temp = angular.fromJson(Auth.getFields());
                            $rootScope.consumerId = temp.consumerId;
                            $rootScope.fullName = temp.fullName;
                            $rootScope.thumbnail = temp.thumbnail;
                        }
                        //hello(r.network).api('me').on('complete', function(r){  
                        //          window.location.href = "#/app/products";
                        //});	 
                        window.location.href = "#/app/products";
                    }
                    $rootScope.loader = false;
                }).
                error(function (data, status, headers, config) {
                    $rootScope.error_code = status;
                    $rootScope.error_messge = "error occured while login.";

                    $rootScope.loader = false;

                });

            //this.logdataa(r);


        }

    }
}]);
//Swiper plugin
myApp.factory('swiperPlugin', function ($rootScope) {
    return {
        swipein: function (c, s) {
            mySwiper = new Swiper('.' + c, {
                slidesPerView: s,
                watchActiveIndex: true,
                grabCursor: true,
                onTouchEnd: function () {

                },
                onSlideClick: function (event) {

                    idv = event.clickedSlideIndex + 1;

                    mySwiper.swipeTo(event.clickedSlideIndex, 1000, true);
                }
            });
        }
    }
});


/*//mobile menu
 myApp.directive("mobileMenu", [function () {
 return{
 restrict: "A",
 link: function (scope, element, attr) {
 jQuery(".filter-icon").on("click",function(){
 jQuery(".filter-list-wrap").addClass("mobile-animate");
 });
 jQuery(".filter-list-wrap .close-overlay").on("click",function(){
 $(".filter-list-wrap").removeClass("mobile-animate");
 });
 jQuery(".right-arrow").on("click",function(){
 jQuery(".subcategory-list").addClass("mobilesub-animate");
 });
 jQuery(".breadcrumb-back,.subcategory-list .close-overlay").on("click",function(){
 jQuery(".subcategory-list").removeClass("mobilesub-animate");
 });
 }
 }

 }]);*/



//sub menu
myApp.directive("subMenu", [function () {
    return {
        restrict: "A",
        link: function (scope, element, attr) {
            jQuery(".submenu-click").on("click", function () {
                jQuery(".productsubmenu-wrap").show();
            });
            jQuery(".remove-submenu").on("click", function () {
                jQuery(".productsubmenu-wrap").hide();
            });
        }
    }

}]);

//category-height
myApp.directive("categoryHeight", [function () {
    return {
        restrict: "A",
        link: function (scope, element, attr) {
            jQuery(".filter-list-wrap,.subcategory-list").css('height', jQuery(".all-items").height() + 100);
        }
    }

}]);


//product tabs
myApp.directive("productTabs", [function () {
    return {
        restrict: "A",
        link: function (scope, element, attr) {
            jQuery(".main-tabs li").click(function () {
                //alert();
                jQuery(".main-tabs li").removeClass("current");
                jQuery(this).addClass("current");
                jQuery(".tab-content").hide();
                var x = jQuery(this).find("a").attr("data-tabs"); //Find the rel attribute value to identify the active tab + content
                jQuery(x).fadeIn(); //Fade in the active content
                return false;
            });
        }
    }

}]);

//inner-tab
myApp.directive("innerTab", [function () {
    return {
        restrict: "A",
        link: function (scope, element, attr) {
            jQuery(".tab-wrap li").click(function () {
                //alert();
                jQuery(".tab-wrap li").removeClass("tab-active");
                jQuery(this).addClass("tab-active");
                jQuery(".innertab-content").hide();
                var x = jQuery(this).find("a").attr("data-innertabs"); //Find the rel attribute value to identify the active tab + content
                jQuery(x).fadeIn(); //Fade in the active content
                return false;
            });
        }
    }

}]);

myApp.directive("brewpointsTabs", [function () {
    return {
        restrict: "A",
        link: function (scope, element, attr) {
            jQuery(".brew-wrap-item-right li").click(function () {
                jQuery(".brew-wrap-item-right li").removeClass("brew-item-active");
                jQuery(this).addClass("brew-item-active");
                jQuery(".brew-item-data").hide();
                var x = jQuery(this).find("a").attr("data-tabs"); //Find the rel attribute value to identify the active tab + content
                jQuery(x).fadeIn(100); //Fade in the active content
                return false;
            });
        }
    }

}])

    .directive('youtube', function ($sce) {

        return {

            restrict: 'EA',
            template: '<iframe width="100%" height="300" src="{{url}}" frameborder="0" allowfullscreen></iframe>',
            scope: {
                youtube: '='
            },
            link: function (scope, element, attr) {
                scope.$watch('youtube', function (source) { // use newVal and oldVal
                    console.log(source); // log it to see that it is passed

                    scope.url = $sce.trustAsResourceUrl(source + '?rel=0&modestbranding=1&autohide=1&showinfo=1');

                });

                //scope.url =scope.youtube.source;
                //console.log(scope.youtube.source);
            }
        };
    })
    .directive('youtubeVideo', function ($sce) {

        return {
            restrict: 'EA',
            template: '<div ng-click="movie.show=!movie.show" ng-show="!movie.show" class="youtube-poster"><a class="play-button" href="javascript:;"></a><img ng-src="{{imgUrl}}" ng-if="imgUrl"></div>' +
            '<div ng-if="movie.show">' +
            '<iframe width="100%" height="300" ng-src="{{videoUrl}}" frameborder="0" allowfullscreen></iframe></div>',
            scope: {
                youtubeVideo: '='
            },
            link: function (scope, element, attr) {
                scope.$watch('youtubeVideo', function (source) { // use newVal and oldVal
                    if (source != undefined) {
                        console.log(source); // log it to see that it is passed
                        var regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=)([^#\&\?]*).*/;
                        var match = source.match(regExp);
                        var videoCode = '#';
                        if (match && match[2].length == 11) {
                            videoCode = match[2];
                        }
                        scope.imgUrl = $sce.trustAsResourceUrl('https://img.youtube.com/vi/' + videoCode + '/1.jpg');
                        scope.videoUrl = $sce.trustAsResourceUrl('https://www.youtube.com/embed/' + videoCode + '?&autoplay=1&rel=0&modestbranding=1&autohide=1&showinfo=1');
                    }
                });

                //scope.url =scope.youtube.source;
                //console.log(scope.youtube.source);
            }
        };
    })
    .directive('youtubeImg', function ($sce) {
        return {
            restrict: 'EA',
            template: '<a class="play-button" href="javascript:;" ng-click="getIframe({{code}})"></a><img src="{{url}}" />',
            scope: {
                youtubeImg: '='
            },
            link: function (scope, element, attr) {

                scope.$watch('youtubeImg', function (source) { // use newVal and oldVal
                    console.log(source); // log it to see that it is passed

                    var regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=)([^#\&\?]*).*/;
                    var match = source.match(regExp);

                    if (match && match[2].length == 11) {
                        videoCode = match[2];
                    } else {
                        videoCode = '';
                    }
                    console.log(videoCode);
                    scope.code = videoCode;
                    scope.url = $sce.trustAsResourceUrl('http://img.youtube.com/vi/' + videoCode + '/0.jpg');
                });

                //console.log(scope.youtube.source);
            }
        };
    })
    .directive('fallbackSrc', function () {
        var fallbackSrc = {
            link: function postLink(scope, iElement, iAttrs) {
                iElement.bind('error', function () {
                    angular.element(this).attr("src", iAttrs.fallbackSrc);
                });
            }
        }
        return fallbackSrc;
    })
    .directive("scrollable", function () {
        return function (scope, element, attrs) {
            var container = angular.element(element);
            container.bind("scroll", function (evt) {
                console.log(evt);
                if (container[0].scrollTop <= 0) {
                    console.log('col-', container[0].scrollTop);
                    //alert('On the top of the world I\'m singing I\'m dancing.');
                }
            });
        };
    })
    .directive('scrollWatch', function ($rootScope) {
        return function (scope, elem, attr) {
            var start = 0;
            var threshold = 150;
            elem.bind('scroll', function (e) {
                //console.log(e.detail.scrollTop - start);
                if (e.detail.scrollTop - start > threshold) {
                    $rootScope.sttButton = true;
                    $rootScope.slideHeader = true;
                } else {
                    $rootScope.slideHeader = false;
                    $rootScope.sttButton = false;
                }
                if ($rootScope.slideHeaderPrevious >= e.detail.scrollTop - start) {
                    $rootScope.slideHeader = false;
                }

                $rootScope.slideHeaderPrevious = e.detail.scrollTop - start;
                if ($rootScope.$$phase != '$apply' && $rootScope.$$phase != '$digest') {
                    $rootScope.$apply();
                }
            });
        };
    })

//.directive('loading',   ['$http' ,function ($http)
//{
//    return {
//        restrict: 'A',
//        link: function (scope, elm, attrs)
//        {
//            scope.isLoading = function () {
//                return $http.pendingRequests.length > 0;
//            };
//
//            scope.$watch(scope.isLoading, function (v)
//            {
//                if(v){
//                    $(elm).show();
//                }else{
//                    $(elm).hide();
//                }
//            });
//        }
//    };
//
//}]);























 
      
    
