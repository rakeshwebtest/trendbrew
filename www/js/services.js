angular.module('starter.services', [])

/**
 * A simple example service that returns some data.
 */
    .factory('Friends', function () {
        // Might use a resource here that returns a JSON array

        // Some fake testing data
        var friends = [
            {id: 0, name: 'Scruff McGruff'},
            {id: 1, name: 'G.I. Joe'},
            {id: 2, name: 'Miss Frizzle'},
            {id: 3, name: 'Ash Ketchum'}
        ];

        return {
            all: function () {
                return friends;
            },
            get: function (friendId) {
                // Simple index lookup
                return friends[friendId];
            }
        }
    })
    .factory('category', function ($http, $cacheFactory, $rootScope) {
        var categories = [];


        return {
            get: function () {
                var params = '';
                if ($rootScope.pid != null && $rootScope.pid != '' && $rootScope.pid != 'undefined') {
                    params += '&persona=' + $rootScope.pid;
                }
                if ($rootScope.rid != null && $rootScope.rid != '' && $rootScope.rid != 'undefined') {
                    params += '&retailer=' + $rootScope.rid;
                }
                if ($rootScope.bid != null && $rootScope.bid != '' && $rootScope.bid != 'undefined') {
                    params += '&brand=' + $rootScope.bid;
                }
                return $http.get(serviceURL + 'categories?' + params, {cache: true}).then(function (response) {
                    // console.log(response.data.response);
                    return response.data.response;
                });

            }
        }
    })
    .factory('retailers', function ($http, $cacheFactory, $rootScope) {
        return {
            get: function () {
                var params = '';
                if ($rootScope.pid != null && $rootScope.pid != '' && $rootScope.pid != 'undefined') {
                    params += '&persona=' + $rootScope.pid;
                }
                if ($rootScope.cid != null && $rootScope.cid != '' && $rootScope.cid != 'undefined') {
                    params += '&category=' + $rootScope.cid;
                }
                if ($rootScope.bid != null && $rootScope.bid != '' && $rootScope.bid != 'undefined') {
                    params += '&brand=' + $rootScope.bid;
                }

                return $http.get(serviceURL + 'providers?' + params, {cache: true}).then(function (response) {
                    // console.log(response.data.response);
                    return response.data.response;
                });

            }
        }
    })
    .factory('brands', function ($http, $cacheFactory, $rootScope) {
        return {
            get: function () {
                var params = '';
                if ($rootScope.pid != null && $rootScope.pid != '' && $rootScope.pid != 'undefined') {
                    params += '&persona=' + $rootScope.pid;
                }
                if ($rootScope.cid != null && $rootScope.cid != '' && $rootScope.cid != 'undefined') {
                    params += '&category=' + $rootScope.cid;
                }
                if ($rootScope.rid != null && $rootScope.rid != '' && $rootScope.rid != 'undefined') {
                    params += '&retailer=' + $rootScope.rid;
                }

                return $http.get(serviceURL + 'manufacturers?' + params, {cache: true}).then(function (response) {
                    return response.data.response;
                });

            }
        }
    })

    .factory('styles', function ($http, $cacheFactory, $rootScope) {
        var styles = [
            {id: 2, name: 'Female Student', itemType: 'style', icon: 'student-female.jpg'},
            {id: 3, name: 'Young Parent', itemType: 'style', icon: 'youngparent-female.jpg'},
            {id: 5, name: 'Soccer Parent', itemType: 'style', icon: 'soccer-mom.jpg'},
            {id: 7, name: 'Male Executive', itemType: 'style', icon: 'working-male.jpg'},
            {id: 8, name: 'Female Executive', itemType: 'style', icon: 'fashionista.jpg'},
            {id: 9, name: 'Fashionista', itemType: 'style', icon: 'highroller-male.jpg'},
            {id: 10, name: 'Male High Roller', itemType: 'style', icon: 'highroller-female.jpg'},
            {id: 11, name: 'Female High Roller', itemType: 'style', icon: 'homeorganizer.jpg'},
            {id: 12, name: 'Home Organizer', itemType: 'style', icon: 'techie.jpg'},
            {id: 13, name: 'Techme', itemType: 'style', icon: 'gamer.jpg'},
            {id: 14, name: 'Gamer', itemType: 'style', icon: 'active-male.jpg'},
            {id: 15, name: 'Active Male', itemType: 'style', icon: 'active-female.jpg'},
            {id: 16, name: 'Active Female', itemType: 'style', icon: 'booklover.jpg'},
            {id: 17, name: 'Book Lover', itemType: 'style', icon: 'bargain_shopper.png'},
            {id: 18, name: 'Bargain Shopper', itemType: 'style', icon: 'stylishdude.jpg'},
            {id: 20, name: 'Flamboyant', itemType: 'style', icon: 'petlover.jpg'}
        ];

        return {
            get: function () {
                return styles;
            }
        }
    })
    .factory('mainServices', function ($http, $cacheFactory, $rootScope, $q) {
        return {
            product:{
                get:function($limit,$paramData,$serviceName){
                    return $http.get(serviceURL + $serviceName+'?count=' + $rootScope.limitCount + '&from=' + $limit + $paramData, {cache: true})
                        .error(function () {
                        $rootScope.loader = false;
                        $rootScope.showErrAlert();
                    }).then(function (response) {
                        $rootScope.loader = false;
                        return response.data;
                    });
                },
                productDetails:function($productId){
                    $rootScope.loader = true;
                    return $http.get(serviceURL + 'product?id=' +$productId, {cache: true})
                        .error(function () {
                            $rootScope.loader = false;
                            $rootScope.showErrAlert();
                        }).then(function (response) {
                            $rootScope.loader = false;
                            return response.data;
                        });
                }
            },
            topBrewers: function ($limit, $brewerId) {
                $rootScope.loader = true;
                if ($brewerId != '' && $brewerId != undefined) {
                    $params = '&brewerId=' + $brewerId;
                }
                return $http.get(serviceURL + 'topBrewers?count=' + $rootScope.limitCount + '&from=0' + $params, {cache: true}).error(function () {
                    $rootScope.loader = false;
                    $rootScope.showErrAlert();
                }).then(function (response) {
                    $rootScope.loader = false;
                    return response.data.response;
                });
            },
            topVideos: function ($limit, $sortType) {
                // $rootScope.loader = true;
                var $params = '';
                if ($sortType != '' && $sortType != undefined) {
                    $params = '&sortType=' + $sortType;
                }
                return $http.get(serviceURL + 'topVideos?count=' + $rootScope.limitCount + '&from=' + $limit + $params, {cache: true}).error(function () {
                    // $rootScope.loader = false;
                    $rootScope.showErrAlert();
                }).then(function (response) {
                    //  $rootScope.loader = false;
                    return response.data.response;
                });
            },
            topBlogs: function ($limit, $sortType) {
                //$rootScope.loader = true;
                var $params = '';
                if ($sortType != '' && $sortType != undefined) {
                    $params = '&sortType=' + $sortType;
                }
                return $http.get(serviceURL + 'topBlogs?count=' + $rootScope.limitCount + '&from=' + $limit + $params, {cache: true}).error(function () {
                    // $rootScope.loader = false;
                    $rootScope.showErrAlert();
                }).then(function (response) {
                    //$rootScope.loader = false;
                    return response.data.response;
                });
            },
            brewerBrewers: function ($brewerId) {
                $rootScope.loader = true;
                return $http.get(serviceURL + 'brewer/brews?brewerId=' + $brewerId, {cache: true}).error(function () {
                    $rootScope.loader = false;
                    $rootScope.showErrAlert();
                }).then(function (response) {
                    $rootScope.loader = false;
                    return response.data.response;
                });
            },
            brewerTrends: function ($brewerId) {
                $rootScope.loader = true;
                return $http.get(serviceURL + 'storeItems?storeId=' + $brewerId, {cache: true}).error(function () {
                    $rootScope.loader = false;
                    $rootScope.showErrAlert();
                }).then(function (response) {
                    $rootScope.loader = false;
                    return response.data.response;
                });
            },
            videoDetail: function ($videoId) {
                $rootScope.loader = true;
                return $http.get(serviceURL + 'video?id=' + $videoId, {cache: true}).error(function () {
                    $rootScope.loader = false;
                    $rootScope.showErrAlert();
                }).then(function (response) {
                    $rootScope.loader = false;
                    return response.data.response;
                });
            },
            productVideos: function ($productId) {
                $rootScope.loader = true;
                return $http.get(serviceURL + 'product/videos?productId=' + $productId, {cache: true}).error(function () {
                    $rootScope.loader = false;
                    $rootScope.showErrAlert();
                }).then(function (response) {
                    $rootScope.loader = false;
                    return response.data.response;
                });
            },
            productBlogs: function ($productId) {
                $rootScope.loader = true;
                return $http.get(serviceURL + 'product/blogs?productId=' + $productId, {cache: true}).error(function () {
                    $rootScope.loader = false;
                    $rootScope.showErrAlert();
                }).then(function (response) {
                    $rootScope.loader = false;
                    return response.data.response;
                });
            },
            addBlog: function ($productId, $blogData) {
                var params = {};
                params.itemId = $productId;
                params.source = $blogData.url;
                params.name = $blogData.title;
                $rootScope.loader = true;
                return $http.post(serviceURL + 'blog/add', params).error(function () {
                    $rootScope.loader = false;
                    $rootScope.showErrAlert();
                }).then(function (response) {
                    $rootScope.loader = false;
                    return response;
                });
            },
            addVideo: function ($productId, $videoData) {
                var params = {};
                params.itemId = $productId;
                params.source = $videoData.url;
                params.name = $videoData.title;
                $rootScope.loader = true;
                return $http.post(serviceURL + 'video/add', params).error(function () {
                    $rootScope.loader = false;
                    $rootScope.showErrAlert();
                }).then(function (response) {
                    $rootScope.loader = false;
                    return response;
                });

            },
            shopSearch: function ($key, $limitFrom) {
                return $http.get(serviceURL + 'providers?startsWith=' + $key + '&count=' + $rootScope.limitCount).error(function () {
                    $rootScope.loader = false;
                    $rootScope.showErrAlert();
                }).then(function (response) {
                    $rootScope.loader = false;
                    return response.data;
                });
            },
            productSearch: function ($key) {
                return $http.get(serviceURL + 'product/search?q=' + $key + '&count=' + $rootScope.limitCount).error(function () {
                    $rootScope.loader = false;
                    $rootScope.showErrAlert();
                }).then(function (response) {
                    $rootScope.loader = false;
                    return response.data;
                });
            },
            addToBookmark: function (url) {
                return $http.get(serviceURL + 'product/bookmark?url=' + url).error(function () {
                    $rootScope.loader = false;
                    $rootScope.showErrAlert();
                }).then(function (response) {
                    $rootScope.loader = false;
                    return response.data;
                });
            },
            externalUrlCheckout: function (dealId,consumerId) {
                var params = {};
                params.offerId = dealId;
                params.brewerId = $rootScope.consumerId;
                    return $http.post(serviceURL + 'product/checkout/intention', params).error(function () {
                        $rootScope.loader = false;
                        $rootScope.showErrAlert();
                    }).then(function (response) {
                        $rootScope.loader = false;
                        return response;
                    });
            },
            brewTrendAdd: function (obj) {
                var params = obj;
                $rootScope.loader = true;
                return $http.post(serviceURL + 'trends/addMyTrend', params).error(function () {
                    $rootScope.loader = false;
                    $rootScope.showErrAlert();
                }).then(function (response) {
                    $rootScope.loader = false;
                    return response.data.response;
                });
            }
        }
    })

    .factory('profile', function ($http, $cacheFactory, $rootScope) {

        return {
            get: function ($brewerId) {
                $rootScope.loader = true;
                return $http.get(serviceURL + 'brewer/profile?brewerId=' + $brewerId, {cache: false}).error(function () {
                    $rootScope.loader = false;
                    $rootScope.showErrAlert();
                }).then(function (response) {
                    $rootScope.loader = false;
                    //console.log(response.data.response);
                    return response.data.response;
                });
            },
            profileUpdate: function ($brewerData) {
                console.log($brewerData);
                $rootScope.loader = true;
                return $http.post(serviceURL + 'brewer/update', $brewerData).error(function () {
                    $rootScope.loader = false;
                }).then(function (response) {
                    $rootScope.loader = false;
                    return response;
                });
            },
            getBrewerDetails: function ($brewerId, $consumerId) {
                var param = '';
                if ($consumerId != '') {
                    param = '&currentBrewer=' + $consumerId;
                }
                return $http.get(serviceURL + 'brewer/profile?brewerId=' + $brewerId + param, {cache: false}).error(function () {
                    $rootScope.loader = false;
                    $rootScope.showErrAlert();
                }).then(function (response) {
                    //console.log(response.data.response);
                    return response.data.response;
                });
            },
            follow: function ($consumerId, $brewerId) {
                return $http.post(serviceURL + 'brewer/follow', {
                    "brewerId": $brewerId.toString(),
                    "followedId": $consumerId.toString()
                }).error(function () {
                    $rootScope.loader = false;
                }).then(function (response) {
                    $rootScope.loader = false;
                    return response;
                });
            },
            unFollow: function ($consumerId, $brewerId) {
                return $http.post(serviceURL + 'brewer/unfollow', {
                    "brewerId": $brewerId.toString(),
                    "followedId": $consumerId.toString()
                }).error(function () {
                    $rootScope.loader = false;
                }).then(function (response) {
                    $rootScope.loader = false;
                    return response;
                });
            },
            store: function ($brewerId) {
                return $http.get(serviceURL + 'brewer/storeTransactions?brewerId=' + $brewerId, {cache: true}).error(function () {
                    $rootScope.loader = false;
                    $rootScope.showErrAlert();
                }).then(function (response) {
                    //console.log(response.data.response);
                    return response.data.response;
                });
            },
            wishes: function ($brewerId) {
                return $http.get(serviceURL + 'brewer/wishes?brewerId=' + $brewerId, {cache: true}).error(function () {
                    $rootScope.loader = false;
                    $rootScope.showErrAlert();
                }).then(function (response) {
                    return response.data.response;
                });
            },
            feeds:
            {
                get: function ($brewerId, $limit) {
                    return $http.get(serviceURL + 'brewer/feed?from=' + $limit + '&count=' + $rootScope.limitCount + '&brewerId=' + $brewerId, {cache: false}).error(function () {
                        $rootScope.loader = false;
                        $rootScope.showErrAlert();
                    }).then(function (response) {
                        return response.data;
                    });
                },
                like:function($brewerId,$eventId){
                    return $http.get(serviceURL + 'brewfeed/brewlike?eventId='+$eventId).error(function () {
                        $rootScope.loader = false;
                    }).then(function (response) {
                        $rootScope.loader = false;
                        return response;
                    });

                },
                comment:function($brewerId,$eventId,$comment){
                    return $http.post(serviceURL + 'brewfeed/comment', {
                        "brewerId": $brewerId.toString(),
                        "eventId": $eventId.toString(),
                        "comment": $comment,
                    }).error(function () {
                        $rootScope.loader = false;
                    }).then(function (response) {
                        $rootScope.loader = false;
                        return response;
                    });

                }
            },
            followers: function ($brewerId) {
                return $http.get(serviceURL + 'brewer/followers?brewerId=' + $brewerId, {cache: true}).error(function () {
                    $rootScope.loader = false;
                    $rootScope.showErrAlert();
                }).then(function (response) {
                    return response.data.response;
                });
            },
            following: function ($brewerId) {
                return $http.get(serviceURL + 'brewer/followed?brewerId=' + $brewerId, {cache: true}).error(function () {
                    $rootScope.loader = false;
                    $rootScope.showErrAlert();
                }).then(function (response) {
                    return response.data.response;
                });
            },
            offers: function ($brewerId) {
                return $http.get(serviceURL + 'brewer/offers?brewerId=' + $brewerId, {cache: true}).error(function () {
                    $rootScope.loader = false;
                    $rootScope.showErrAlert();
                }).then(function (response) {
                    return response.data.response;
                });
            },
            brewPoints: function ($brewerId) {
                $rootScope.loader = true;
                return $http.get(serviceURL + 'brewer/brewPoints?brewerId=' + $brewerId, {cache: true}).error(function () {
                    $rootScope.loader = false;
                    $rootScope.showErrAlert();
                }).then(function (response) {
                    $rootScope.loader = false;
                    return response.data.response;
                });
            },
            pointsTransactions: function ($brewerId) {
                $rootScope.loader = true;
                return $http.get(serviceURL + 'brewer/pointsTransactions?brewerId=' + $brewerId, {cache: true}).error(function () {
                    $rootScope.showErrAlert();
                }).then(function (response) {
                    return response.data.response;
                });
            },
            //purchase transations
            myTransaction: function ($brewerId) {
                $rootScope.loader = true;
                return $http.get(serviceURL + 'brewer/myTransactions?brewerId=' + $brewerId, {cache: true}).error(function () {
                    $rootScope.loader = false;
                    $rootScope.showErrAlert();
                }).then(function (response) {
                    console.log(response.data.response);
                    $rootScope.loader = false;
                    return response.data.response;
                });
            },
            storeTransactions: function ($brewerId) {
                $rootScope.loader = true;
                return $http.get(serviceURL + 'brewer/storeTransactions?brewerId=' + $brewerId, {cache: true}).error(function () {
                    $rootScope.loader = false;
                    $rootScope.showErrAlert();
                }).then(function (response) {
                    console.log(response.data.response);
                    return response.data.response;
                });
            },
            /*
             {"source":"http://www.thegardenglove.com/front-porch-halloween-decorating-ideas/","name":"Font Porch Halloween","items":{"24954":"button front faux suede a-line skirt","25799":" 'With Love' Babydoll Dress"}, "brewerId":6}

             Items is a map of itemId to itemName. This is an optional field.
             Required fields are source,name and brewerId
             */
            addVideo: function ($brewerId, $videoData) {
                var params = {};
                params.brewerId = $brewerId;
                params.source = $videoData.url;
                params.name = $videoData.title;
                $rootScope.loader = true;
                return $http.post(serviceURL + 'video/add', params).error(function () {
                    $rootScope.loader = false;
                    $rootScope.showErrAlert();
                }).then(function (response) {
                    $rootScope.loader = false;
                    return response;
                });

            },
            addBlog: function ($brewerId, $blogData) {
                var params = {};
                params.brewerId = $brewerId;
                params.source = $blogData.url;
                params.name = $blogData.title;

                $rootScope.loader = true;
                return $http.post(serviceURL + 'blog/add', params).error(function () {
                    $rootScope.loader = false;
                    $rootScope.showErrAlert();
                }).then(function (response) {
                    $rootScope.loader = false;
                    return response;
                });
            },
            brewerVideos: function ($brewerId) {
                $rootScope.loader = true;
                return $http.get(serviceURL + 'videos?brewerId=' + $brewerId, {cache: true}).error(function () {
                    $rootScope.loader = false;
                    $rootScope.showErrAlert();
                }).then(function (response) {
                    $rootScope.loader = false;
                    return response.data.response;
                });
            },
            brewerStores: function ($brewerId) {
                $rootScope.loader = true;
                return $http.get(serviceURL + 'brewer/stores?brewerId=' + $brewerId, {cache: true}).error(function () {
                    $rootScope.loader = false;
                    $rootScope.showErrAlert();
                }).then(function (response) {
                    $rootScope.loader = false;
                    return response.data.response;
                });
            },
            myBlogs: function ($brewerId) {
                $rootScope.loader = true;
                return $http.get(serviceURL + 'blogs?brewerId=' + $brewerId, {cache: true}).error(function () {
                    $rootScope.loader = false;
                    $rootScope.showErrAlert();
                }).then(function (response) {
                    $rootScope.loader = false;
                    return response.data.response;
                });
            },
            productReview: function ($brewerId, $reviewData) {
                var params = {};
                params.reviewerId = $brewerId;
                params.itemId = $reviewData.itemId;
                params.rating = $reviewData.rating;
                params.review = $reviewData.review;

                $rootScope.loader = true;
                return $http.post(serviceURL + 'product/reviews/add', params).error(function () {
                    $rootScope.loader = false;
                    $rootScope.showErrAlert();
                }).then(function (response) {
                    $rootScope.loader = false;
                    return response.data.response;
                });
            }
        }
    })
    /*

     Takes following parameters
     brewerId - id of the brewer (if null, its treated as a anonymous BrewLike)
     itemId - id of the item being brewed (required)
     brewContext - BrewTrend, BrewWish, BrewLike, BrewBuzz (if not passed defaults to BrewLike)
     myPrice - your wish price
     muDuration - your wish duration
     myComment - Your wish note
     buzz - Social Buzz to be posted
     postToFacebook - Boolean flag for permission to post to facebook
     postToTwitter - Boolean flag for permission to post to twitter
     */
    .factory('addMyTrend', function ($http, $cacheFactory, $rootScope) {
        return {
            brewLike: function ($brewerId, $itemId) {
                var params = {};
                if ($brewerId != '' && $brewerId != undefined) {
                    params.brewerId = $brewerId.toString();
                }
                if ($itemId != '' && $itemId != undefined) {
                    params.itemId = $itemId.toString();
                }
                params.brewContext = 'BrewLike';
                $rootScope.loader = true;
                return $http.post(serviceURL + 'trends/addMyTrend', params).error(function () {
                    $rootScope.loader = false;
                    $rootScope.showErrAlert();
                }).then(function (response) {
                    $rootScope.loader = false;
                    return response;
                });
            },
            BrewTrend: function ($brewerId, $itemId) {
                var params = {};
                if ($brewerId != '' && $brewerId != undefined) {
                    params.brewerId = $brewerId.toString();
                }
                if ($itemId != '' && $itemId != undefined) {
                    params.itemId = $itemId.toString();
                }
                params.brewContext = 'BrewTrend';
                $rootScope.loader = true;
                return $http.post(serviceURL + 'trends/addMyTrend', params).error(function () {
                    $rootScope.loader = false;
                    $rootScope.showErrAlert();
                }).then(function (response) {
                    $rootScope.loader = false;
                    return response;
                });
            },
            BrewWish: function ($brewerId, $itemId, $data) {
                var params = {};
                params.brewContext = "BrewWish";
                if ($brewerId != '' && $brewerId != undefined) {
                    params.brewerId = $brewerId.toString();
                }
                if ($itemId != '' && $itemId != undefined) {
                    params.itemId = $itemId.toString();
                }
                params.myPrice = $data.myPrice;
                params.myDuration = $data.myDuration;
                params.myComment = $data.myComment;
                $rootScope.loader = true;
                return $http.post(serviceURL + 'trends/addMyTrend', params).error(function () {
                    $rootScope.loader = false;
                    $rootScope.showErrAlert();
                }).then(function (response) {
                    $rootScope.loader = false;
                    return response;
                });

            },
            BrewBuzz: function ($brewerId, $itemId, $buzzData) {
                var params = {};
                params.brewContext = "BrewBuzz";
                if ($brewerId != '' && $brewerId != undefined) {
                    params.brewerId = $brewerId.toString();
                }
                if ($itemId != '' && $itemId != undefined) {
                    params.itemId = $itemId.toString();
                }
                // params.myComment=$buzzData.myComment;
                params.buzz = $buzzData.myComment;
                params.postToFacebook = $buzzData.postToFacebook;
                params.postToTwitter = $buzzData.postToTwitter;

                $rootScope.loader = true;
                return $http.post(serviceURL + 'trends/addMyTrend', params).error(function () {
                    $rootScope.loader = false;
                    $rootScope.showErrAlert();
                }).then(function (response) {
                    $rootScope.loader = false;
                    return response;
                });

            }
        }

    })

    .directive('timeAgo', ['timeAgo', 'nowTime', function (timeAgo, nowTime) {
        return {
            restrict: 'EA',
            link: function (scope, elem, attrs) {
                var fromTime;
                // Track the fromTime attribute
                attrs.$observe('fromTime', function (value) {
                    fromTime = timeAgo.parse(value);
                });
                // Track changes to time difference
                scope.$watch(function () {
                    return nowTime() - fromTime;
                }, function (value) {
                    angular.element(elem).text(timeAgo.inWords(value));
                });
            }
        };
    }
    ])
    .factory('nowTime', ['$timeout', function ($timeout) {
        var nowTime = Date.now();
        var updateTime = function () {
            $timeout(function () {
                nowTime = Date.now();
                updateTime();
            }, 1000);
        };
        updateTime();
        return function () {
            return nowTime;
        };
    }
    ])
    .factory('timeAgo', function () {
        var service = {};
        service.settings = {
            refreshMillis: 60000,
            allowFuture: false,
            strings: {
                prefixAgo: null,
                prefixFromNow: null,
                suffixAgo: 'ago',
                suffixFromNow: 'from now',
                seconds: 'less than a minute',
                minute: 'about a minute',
                minutes: '%d minutes',
                hour: 'about an hour',
                hours: 'about %d hours',
                day: 'a day',
                days: '%d days',
                month: 'about a month',
                months: '%d months',
                year: 'about a year',
                years: '%d years',
                numbers: []
            }
        };
        service.inWords = function (distanceMillis) {
            var $l = service.settings.strings;
            var prefix = $l.prefixAgo;
            var suffix = $l.suffixAgo;
            if (service.settings.allowFuture) {
                if (distanceMillis < 0) {
                    prefix = $l.prefixFromNow;
                    suffix = $l.suffixFromNow;
                }
            }
            var seconds = Math.abs(distanceMillis) / 1000;
            var minutes = seconds / 60;
            var hours = minutes / 60;
            var days = hours / 24;
            var years = days / 365;

            function substitute(stringOrFunction, number) {
                var string = angular.isFunction(stringOrFunction) ? stringOrFunction(number, distanceMillis) : stringOrFunction;
                var value = $l.numbers && $l.numbers[number] || number;
                return string.replace(/%d/i, value);
            }

            var words = seconds < 45 && substitute($l.seconds, Math.round(seconds)) || seconds < 90 && substitute($l.minute, 1) || minutes < 45 && substitute($l.minutes, Math.round(minutes)) || minutes < 90 && substitute($l.hour, 1) || hours < 24 && substitute($l.hours, Math.round(hours)) || hours < 42 && substitute($l.day, 1) || days < 30 && substitute($l.days, Math.round(days)) || days < 45 && substitute($l.month, 1) || days < 365 && substitute($l.months, Math.round(days / 30)) || years < 1.5 && substitute($l.year, 1) || substitute($l.years, Math.round(years));
            var separator = $l.wordSeparator === undefined ? ' ' : $l.wordSeparator;
            return [
                prefix,
                words,
                suffix
            ].join(separator).trim();
        };
        service.parse = function (iso8601) {
            if (angular.isNumber(iso8601)) {
                return parseInt(iso8601, 10);
            }
            var s = iso8601.trim();
            s = s.replace(/\.\d+/, '');
            // remove milliseconds
            s = s.replace(/-/, '/').replace(/-/, '/');
            s = s.replace(/T/, ' ').replace(/Z/, ' UTC');
            s = s.replace(/([\+\-]\d\d)\:?(\d\d)/, ' $1$2');
            // -04:00 -> -0400
            return new Date(s);
        };
        return service;
    })
    .filter('timeAgo', [
        'nowTime',
        'timeAgo',
        function (nowTime, timeAgo) {
            return function (value) {
                var fromTime = timeAgo.parse(value);
                var diff = nowTime() - fromTime;
                return timeAgo.inWords(diff);
            };
        }
    ]);

